package it.uniroma2.art.stontoling.bundle;

import it.uniroma2.art.semanticturkey.plugin.extpts.PluginInterface;
import it.uniroma2.art.semanticturkey.plugin.extpts.ServiceInterface;
import it.uniroma2.art.stontoling.plugin.STOntoLingPlugin;
import it.uniroma2.art.stontoling.servlet.STOntoLing;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class STOntoLingBundle implements BundleActivator {

	
	public void start(BundleContext context) throws Exception {
		//context.registerService(ServiceInterface.class.getName(), new STOntoLing("stOntoLing"), null);
		
		//aggiunta la gestione dei plugin
		STOntoLingPlugin stOntoLingPlugin = new STOntoLingPlugin("it.uniroma2.art.stOntoLing");
		context.registerService(PluginInterface.class.getName(),stOntoLingPlugin, null);
		context.registerService(ServiceInterface.class.getName(), new STOntoLing("stOntoLing", stOntoLingPlugin), null);
	}

	public void stop(BundleContext context) throws Exception {
		// TODO Auto-generated method stub
	}

}
