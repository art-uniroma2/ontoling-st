package it.uniroma2.art.stontoling.servlet;

import it.uniroma2.art.lw.model.objects.ExclusiveExplorationObject;
import it.uniroma2.art.lw.model.objects.LexicalRelation;
import it.uniroma2.art.lw.model.objects.SearchFilter;
import it.uniroma2.art.lw.model.objects.SearchStrategy;
import it.uniroma2.art.lw.model.objects.SearchWord;
import it.uniroma2.art.lw.model.objects.SemanticRelation;
import it.uniroma2.art.ontoling.model.OntResourceType;
import it.uniroma2.art.ontoling.ui.CMOption;
import it.uniroma2.art.ontoling.ui.CMOptionList;
import it.uniroma2.art.ontoling.ui.GUIAdapter;
import it.uniroma2.art.ontoling.ui.UIAction;
import it.uniroma2.art.ontoling.ui.UIAction.ContextMenuSetter;
import it.uniroma2.art.ontoling.ui.UIAction.GlossTextSetter;
import it.uniroma2.art.ontoling.ui.UIAction.RelationExplorerSetter;
import it.uniroma2.art.ontoling.ui.UIAction.ResultTableHeadersSetter;
import it.uniroma2.art.ontoling.ui.UIAction.ResultTableSetter;
import it.uniroma2.art.ontoling.ui.UIAction.SearchFilterMarker;
import it.uniroma2.art.ontoling.ui.UIAction.SearchFiltersSetter;
import it.uniroma2.art.ontoling.ui.UIAction.SearchTextSetter;
import it.uniroma2.art.ontoling.ui.UIAction.SearchWordsSetter;
import it.uniroma2.art.ontoling.ui.UIAction.SemanticIndexTextSetter;
import it.uniroma2.art.ontoling.ui.UIAction.SynonymsListSetter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public class STOntoLingGUIAdapter extends GUIAdapter {
	
	private HashMap<Class<? extends ExclusiveExplorationObject>, Collection<String>> relationExplorers;
	private HashMap<String, Collection<CtxMenuItem>> ctxMenuLabels;
	private Collection<SearchWord> searchWordList;
	private Collection<String []> resultTable;
		
	private Collection<SearchFilter> searchFilterList;
	private Collection<String> synonymsList;
	
	public STOntoLingGUIAdapter() {
		relationExplorers = new HashMap<Class<? extends ExclusiveExplorationObject>, Collection<String>>();
		relationExplorers.put(SemanticRelation.class, new ArrayList<String>());
		relationExplorers.put(LexicalRelation.class, new ArrayList<String>());
		relationExplorers.put(SearchStrategy.class, new ArrayList<String>());
		searchFilterList = new ArrayList<SearchFilter>();
		searchWordList = new ArrayList<SearchWord>();
		ctxMenuLabels = new HashMap<String, Collection<CtxMenuItem>>();
		ctxMenuLabels.put("class", new ArrayList<CtxMenuItem>());
		ctxMenuLabels.put("property", new ArrayList<CtxMenuItem>());
	}
	 
	@Override
	protected void carryUIAction(SearchTextSetter sts) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void carryUIAction(GlossTextSetter gts) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void carryUIAction(SemanticIndexTextSetter cts) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void carryUIAction(SynonymsListSetter sls) {
		synonymsList = sls.getSynonyms();
	}

	@Override
	protected void carryUIAction(ResultTableSetter rts) {
		resultTable = rts.getResultsList();
	}

	@Override
	protected void carryUIAction(ResultTableHeadersSetter rths) {
		// TODO Auto-generated method stub

	}

	@Override
	protected <T extends ExclusiveExplorationObject> void carryUIAction(RelationExplorerSetter<T> res) {
		Collection<T> relations = res.getRelations();
		Collection<String> list = relationExplorers.get( res.getType() );
		list.clear();
		if (relations != null) {
            for (T relation : relations)
            	list.add(relation.getName());
        }
		//System.out.println("$$$$ relationExplorers = "+relationExplorers); // da cancellare
	}

	@Override
	protected void carryUIAction(SearchFiltersSetter sfs) {
		searchFilterList = sfs.getSearchFilters();
		if(searchFilterList == null){
			searchFilterList = new ArrayList<SearchFilter>();
		}
	}

	@Override
	protected void carryUIAction(SearchFilterMarker sfm) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void carryUIAction(ContextMenuSetter ctxSetter) {
		CMOptionList ctxMenu = ctxSetter.getContextMenu();
		Collection<CtxMenuItem> ctxMenuItemList = null;
		
		if (ctxMenu.getResType()==OntResourceType.cls) {
			ctxMenuLabels.get("class").clear();
			ctxMenuItemList = ctxMenuLabels.get("class");
		}
		else if (ctxMenu.getResType()==OntResourceType.prop) { 
        	ctxMenuLabels.get("property").clear();
        	ctxMenuItemList = ctxMenuLabels.get("property");
		}
        else
        	return;
        for (CMOption opt : ctxMenu) {
			if (opt instanceof CMOption.AddGloss) {
				ctxMenuItemList.add(new CtxMenuItem("addGloss", opt.getLabel())); 
			}
			else if (opt instanceof CMOption.AddTerms) {
				ctxMenuItemList.add(new CtxMenuItem("addTerm", opt.getLabel())); 
			}
			else if (opt instanceof CMOption.MenuSeparator) {
				ctxMenuItemList.add(new CtxMenuItem("menuSeparator", opt.getLabel())); 
			}
			else if (opt instanceof CMOption.AddSenseIdToResource) {
				ctxMenuItemList.add(new CtxMenuItem("addSenseIdToResource", opt.getLabel())); 
			}
			else if (opt instanceof CMOption.SearchLinguisticResource) {
				ctxMenuItemList.add(new CtxMenuItem("searchLinguisticResource", opt.getLabel())); 
			}
			else if (opt instanceof CMOption.ChangeNameToSelectedTerm) {
				ctxMenuItemList.add(new CtxMenuItem("changeNameToSelectedTerm", opt.getLabel())); 
			}
			else if (opt instanceof CMOption.CreateSubResourceWithSelectedTermAsID) {
				ctxMenuItemList.add(new CtxMenuItem("createSubResourceWithSelectedTermAsID", opt.getLabel())); 
			}
			else if (opt instanceof CMOption.AddSubResourcesUsingSubConceptsFromLinguisticResources) {
				ctxMenuItemList.add(new CtxMenuItem("addSubResourcesUsingSubConceptsFromLinguisticResources", opt.getLabel())); 
			}
			else if (opt instanceof CMOption.CallLinguisticEnrichment) {
				ctxMenuItemList.add(new CtxMenuItem("callLinguisticEnrichment", opt.getLabel())); 
			}
		}

	}

	@Override
	protected void carryUIAction(SearchWordsSetter searchWords) {
		
		Collection<SearchWord> oldSearchWordList = searchWords.getSearchWords();
		
		//It is possible that there are different SearchWord that have the same String, so first of all it's important
    	// to eliminate the duplicates
		
		Collection<String> newSearchWordStringList = new ArrayList<String>();
		Iterator <SearchWord>iteroSWL = oldSearchWordList.iterator();
		while(iteroSWL.hasNext()){
			String searchWordString = iteroSWL.next().getName();
			if(!newSearchWordStringList.contains(searchWordString))
				newSearchWordStringList.add(searchWordString);
		}
		
		Iterator <String> iternSWSL = newSearchWordStringList.iterator();
		searchWordList.clear();
		while(iternSWSL.hasNext()) {
			searchWordList.add(new SearchWord(iternSWSL.next()));
		}
	}

	@Override
	protected void carryUIAction(UIAction sts) {
		// TODO Auto-generated method stub

	}
	
	public HashMap<Class<? extends ExclusiveExplorationObject>, Collection<String>> getRelationExplorers(){
		return relationExplorers;
	}
	
	public Collection<SearchFilter> getSearchFilterList(){
		return searchFilterList;
	}
	
	public HashMap<String, Collection<CtxMenuItem>> getCtxMenuLabels(){
		return ctxMenuLabels;
	}
	
	public Collection<SearchWord> getSearchWordList(){
		return searchWordList;
	}
	
	public Collection<String[]> getResultTable(){
		return resultTable;
	}
	
	public Collection<String> getSynonymsList(){
		return synonymsList;
	}
	
	public static class CtxMenuItem{
		private String label;
		private String name;
		public CtxMenuItem(String name, String label) {
			this.name = name;
			this.label = label;
		}
		
		public String getName(){
			return name;
		}
		
		public String getLabel(){
			return label;
		}
	}

}
