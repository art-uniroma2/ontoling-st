package it.uniroma2.art.stontoling.plugin;

import it.uniroma2.art.semanticturkey.exceptions.PluginDisposedException;
import it.uniroma2.art.semanticturkey.exceptions.PluginInitializationException;
import it.uniroma2.art.semanticturkey.plugin.extpts.PluginAdapter;
import it.uniroma2.art.semanticturkey.plugin.extpts.PluginInterface;

public class STOntoLingPlugin extends PluginAdapter implements PluginInterface {

	public STOntoLingPlugin(String id) {
		super(id);
	}

	@Override
	public boolean dispose() throws PluginDisposedException {
		return true;
	}

	@Override
	public boolean initialize() throws PluginInitializationException {
		return true;
	}

}
