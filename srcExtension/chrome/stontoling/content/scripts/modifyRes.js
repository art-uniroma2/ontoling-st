/*
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at http://www.mozilla.org/MPL/
 * 
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 * 
 * The Original Code is SemanticTurkey.
 * 
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 * 
 * SemanticTurkey was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata (ART) Current
 * information about SemanticTurkey can be obtained at
 * http://semanticturkey.uniroma2.it
 * 
 */
if (typeof art_semanticturkey == 'undefined') var art_semanticturkey = {};
if (!art_semanticturkey.ontoling) art_semanticturkey.ontoling = {};

Components.utils.import("resource://ontolingmodules/stOntoLingModule.jsm", art_semanticturkey.ontoling);
Components.utils.import("resource://ontolingservices/SERVICE_stOntoLing.jsm", art_semanticturkey.ontoling);



window.onload = function() {
	document.getElementById("modifyRes").addEventListener("click",
			art_semanticturkey.ontoling.onModifyRes, true);
	document.getElementById("createRes").addEventListener("click",
			art_semanticturkey.ontoling.onCreateRes, true);
	document.getElementById("removeRes").addEventListener("click",
			art_semanticturkey.ontoling.onRemoveRes, true);
	document.getElementById("close").addEventListener("click",
			art_semanticturkey.ontoling.onClose, true);
	
	var interfaceId = window.arguments[0].interfaceId;
	try {
		var responseXML = art_semanticturkey.ontoling.OLRequests.OntoLing.getLingInstancesList(interfaceId);
		if(responseXML != null)
			art_semanticturkey.ontoling.getLingInstanceList_RESPONSE(responseXML);
	}
	catch (e) {
		alert(e.name + ": " + e.message);
	}
};

art_semanticturkey.ontoling.getLingInstanceList_RESPONSE = function(responseElement){
	var radioGroupModifyRes = document.getElementById("radioGroupModifyRes");

	//First of all we remove all the prevoius elements
	while(radioGroupModifyRes.hasChildNodes())
		radioGroupModifyRes.removeChild(radioGroupModifyRes.childNodes[0]);
	
	//INTERFACE part
	var groupBoxInterfaceTotal = document.createElement("groupbox");
	var captionInterface = document.createElement("caption");
	captionInterface.setAttribute("label", "INTERFACE");
	groupBoxInterfaceTotal.appendChild(captionInterface);
	var hboxInterface = document.createElement("hbox");
	groupBoxInterfaceTotal.appendChild(hboxInterface);
	var radioInterface = document.createElement('radio');
	radioInterface.setAttribute("selected", true);
	hboxInterface.appendChild(radioInterface);
	var groupBoxInterface = document.createElement("groupbox");
	groupBoxInterface.setAttribute("flex", "1");
	hboxInterface.appendChild(groupBoxInterface);
	var vboxInterface = document.createElement("vbox");
	groupBoxInterface.appendChild(vboxInterface);
	var interfaceNode = responseElement.getElementsByTagName("lingInterface")[0];
	var interfaceId = interfaceNode.getAttribute("interfaceId");
	var hboxInterfaceId = document.createElement("hbox");
	vboxInterface.appendChild(hboxInterfaceId);
	var labelInterfaceId = document.createElement("label");
	labelInterfaceId.setAttribute("value", "Interface ID: ");
	hboxInterfaceId.appendChild(labelInterfaceId);
	var textBoxInterfaceid = document.createElement("textbox");
	textBoxInterfaceid.setAttribute("value", interfaceId);
	textBoxInterfaceid.setAttribute("interfaceId", interfaceId);
	textBoxInterfaceid.setAttribute("flex", "1");
	textBoxInterfaceid.setAttribute("readonly", "true");
	hboxInterfaceId.appendChild(textBoxInterfaceid);
	var interfacePropertiesNode = interfaceNode.getElementsByTagName("interfaceProp");
	for(var i=0; i<interfacePropertiesNode.length; ++i){
		var hboxProp = document.createElement("hbox");
		vboxInterface.appendChild(hboxProp);
		var propDescr = interfacePropertiesNode[i].getAttribute("propDescr");
		var propId = interfacePropertiesNode[i].getAttribute("propId");
		var propValue = interfacePropertiesNode[i].getAttribute("propValue");
		var textboxInterfaceProp = document.createElement("textbox");
		var labelId = document.createElement("label");
		labelId.setAttribute("value", propId+": ");
		hboxProp.appendChild(labelId);
		//textboxInterfaceProp.setAttribute("value", propId+": "+propValue+"  ("+propDescr+")");
		textboxInterfaceProp.setAttribute("value", propValue);
		textboxInterfaceProp.setAttribute("propId", propId);
		textboxInterfaceProp.setAttribute("propValue", propValue);
		textboxInterfaceProp.setAttribute("propDescr", propDescr);
		textboxInterfaceProp.setAttribute("flex", "1");
		textboxInterfaceProp.setAttribute("readonly", "true");
		//vboxInterface.appendChild(textboxInterfaceProp);
		hboxProp.appendChild(textboxInterfaceProp);
		var labelDescr = document.createElement("label");
		labelDescr.setAttribute("value", " ("+propDescr+")");
		hboxProp.appendChild(labelDescr);
	}
	radioGroupModifyRes.appendChild(groupBoxInterfaceTotal);
	
	//INSTANCES part
	var groupBoxInstancesTotal = document.createElement("groupbox");
	groupBoxInstancesTotal.setAttribute("flex", "1");
	var captionInstances = document.createElement("caption");
	captionInstances.setAttribute("label", "INSTANCES");
	groupBoxInstancesTotal.appendChild(captionInstances);
	var vboxInstancesTotal = document.createElement("vbox");
	groupBoxInstancesTotal.appendChild(vboxInstancesTotal);
	var instancesNode = responseElement.getElementsByTagName("lingInstance");
	for(var i=0; i<instancesNode.length; ++i){
		var hboxRadioInstance = document.createElement("hbox");
		vboxInstancesTotal.appendChild(hboxRadioInstance);
		var radioInstance = document.createElement('radio');
		hboxRadioInstance.appendChild(radioInstance);
		var groupboxInstance = document.createElement("groupbox");
		groupboxInstance.setAttribute("flex", "1");
		hboxRadioInstance.appendChild(groupboxInstance);
		var vboxInstance = document.createElement("vbox");
		groupboxInstance.appendChild(vboxInstance);
		var instancesId = instancesNode[i].getAttribute("instanceId");
		var hboxInstanceId = document.createElement("hbox");
		vboxInstance.appendChild(hboxInstanceId);
		var labelInstanceId = document.createElement("label");
		labelInstanceId.setAttribute("value", "Interface ID: ");
		hboxInstanceId.appendChild(labelInstanceId);
		textboxInstanceId = document.createElement("textbox");
		textboxInstanceId.setAttribute("value", instancesId);
		textboxInstanceId.setAttribute("instanceId",instancesId);
		radioInstance.setAttribute("instanceId",instancesId);
		textboxInstanceId.setAttribute("flex", "1");
		textboxInstanceId.setAttribute("readonly", "true");
		hboxInstanceId.appendChild(textboxInstanceId);
		var instancePropertiesNode = instancesNode[i].getElementsByTagName("instanceProp");
		for(var k=0; k<instancePropertiesNode.length; ++k){
			var propDescr = instancePropertiesNode[k].getAttribute("propDescr");
			var propId = instancePropertiesNode[k].getAttribute("propId");
			var propValue = instancePropertiesNode[k].getAttribute("propValue");
			var hboxInstanceProp = document.createElement("hbox");
			vboxInstance.appendChild(hboxInstanceProp);
			var labelPropId = document.createElement("label");
			labelPropId.setAttribute("value",propId+": ");
			hboxInstanceProp.appendChild(labelPropId);
			var textInstanceProp = document.createElement("textbox");
			//textInstanceProp.setAttribute("value", propId+": "+propValue+"  ("+propDescr+")");
			textInstanceProp.setAttribute("value", propValue);
			textInstanceProp.setAttribute("propId", propId);
			textInstanceProp.setAttribute("propValue", propValue);
			textInstanceProp.setAttribute("propDescr", propDescr);
			textInstanceProp.setAttribute("flex", "1");
			textInstanceProp.setAttribute("readonly", "true");
			hboxInstanceProp.appendChild(textInstanceProp);
			var labelPropDescr = document.createElement("label");
			labelPropDescr.setAttribute("value", propDescr+": ");
			hboxInstanceProp.appendChild(labelPropDescr);
		}
	}
	radioGroupModifyRes.appendChild(groupBoxInstancesTotal);
	
	//template to add a new instance
	//var buttonCreteRes = document.getElementById("createRes");
	var templateNode = responseElement.getElementsByTagName("lingInstanceTeplate")[0];
	var instancePropTemplateNode = templateNode.getElementsByTagName("instanceProp");
	var propList = new Array();
	for(var i=0; i<instancePropTemplateNode.length; ++i){
		propList[i] = new Object();
		propList[i].propId = instancePropTemplateNode[i].getAttribute("propId");
		propList[i].propDescr = instancePropTemplateNode[i].getAttribute("propDescr");
	}
	document.getElementById("createRes").propList = propList;
};


art_semanticturkey.ontoling.onCreateRes = function(){
	var interfaceId = window.arguments[0].interfaceId;
	var parameters = new Object();
	parameters.okChosen = "false";
	parameters.interfaceId = interfaceId;
	parameters.propList = document.getElementById("createRes").propList;
	window.openDialog("chrome://stontoling/content/createInstance.xul","_blank","modal=yes,resizable,centerscreen", parameters);
	if(parameters.okChosen == "true"){
		try {
			window.arguments[0].changed = "true";
			var responseXML = art_semanticturkey.ontoling.OLRequests.OntoLing.getLingInstancesList(interfaceId);
			if(responseXML != null)
				art_semanticturkey.ontoling.getLingInstanceList_RESPONSE(responseXML);
		}
		catch (e) {
			alert(e.name + ": " + e.message);
		}
	}
		
};

art_semanticturkey.ontoling.onModifyRes = function(){
	var radioGroupModifyRes = document.getElementById("radioGroupModifyRes");
	var selectedItem = radioGroupModifyRes.selectedItem;
	var hboxRadioInstance = selectedItem.parentNode;
	var vboxResInfo = hboxRadioInstance.getElementsByTagName("vbox")[0];
	var isInterface;
	var resId;
	var selectedIndex = radioGroupModifyRes.selectedIndex;
	if(selectedIndex == 0){//the interface was selected
		isInterface = "true";
		resId = vboxResInfo.getElementsByTagName("textbox")[0].getAttribute("interfaceId");
	}
	else{ //one of the instances was selected
		isInterface = "false";
		resId = vboxResInfo.getElementsByTagName("textbox")[0].getAttribute("instanceId");
	}
	var propList = new Array();
	textboxProperties = vboxResInfo.getElementsByTagName("textbox");
	for(var i=1; i<textboxProperties.length; ++i){
		var propId = textboxProperties[i].getAttribute("propId");
		var propDescr = textboxProperties[i].getAttribute("propDescr");
		var propValue = textboxProperties[i].getAttribute("propValue");
		propList[i-1] = new Object();
		propList[i-1].propId = propId;
		propList[i-1].propDescr = propDescr;
		propList[i-1].propValue = propValue;
	}
	
	parameters = new Object();
	parameters.isInterface = isInterface;
	parameters.resId = resId;
	parameters.propList = propList;
	parameters.interfaceId = window.arguments[0].interfaceId;
	parameters.modificationMade = "false";
	window.openDialog("chrome://stontoling/content/modifySingleResource.xul","_blank","modal=yes,resizable,centerscreen", parameters);
	if(parameters.modificationMade == "true"){
		try {
			window.arguments[0].changed = "true";
			var interfaceId = window.arguments[0].interfaceId;
			var responseXML = art_semanticturkey.ontoling.OLRequests.OntoLing.getLingInstancesList(interfaceId);
			if(responseXML != null)
				art_semanticturkey.ontoling.getLingInstanceList_RESPONSE(responseXML);
		}
		catch (e) {
			alert(e.name + ": " + e.message);
		}
	}
};


art_semanticturkey.ontoling.onRemoveRes = function(){
	var radioGroupModifyRes = document.getElementById("radioGroupModifyRes");
	var selectedItem = radioGroupModifyRes.selectedItem;
	var selectedIndex = radioGroupModifyRes.selectedIndex;
	if(selectedIndex== 0){
		alert("It's not possibile do remove the Interface, please select an instance");
		return;
	}
	var instanceId = selectedItem.getAttribute("instanceId");
	var risp = confirm("remove the instance: "+instanceId+" ?");
	if (risp) {
		try {
			window.arguments[0].changed = "true";
			var interfaceId = window.arguments[0].interfaceId;
			var responseXML = art_semanticturkey.ontoling.OLRequests.OntoLing.removeLingInstance(
					interfaceId,
					instanceId);
			if(responseXML != null){
				responseXML = art_semanticturkey.ontoling.OLRequests.OntoLing.getLingInstancesList(interfaceId);
				if(responseXML != null)
					art_semanticturkey.ontoling.getLingInstanceList_RESPONSE(responseXML);
			}
		}
		catch (e) {
			alert(e.name + ": " + e.message);
		}
	}
};

art_semanticturkey.ontoling.onClose = function() {
	close();
};