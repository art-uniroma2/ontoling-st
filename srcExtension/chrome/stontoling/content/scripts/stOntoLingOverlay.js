
/*
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at http://www.mozilla.org/MPL/
 * 
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 * 
 * The Original Code is SemanticTurkey.
 * 
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 * 
 * SemanticTurkey was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata (ART) Current
 * information about SemanticTurkey can be obtained at
 * http://semanticturkey.uniroma2.it
 * 
 */

if (typeof art_semanticturkey == 'undefined')
	var art_semanticturkey = {};
if (!art_semanticturkey.ontoling)
	art_semanticturkey.ontoling = {};

Components.utils.import("resource://ontolingmodules/stOntoLingModule.jsm",
		art_semanticturkey.ontoling);
Components.utils.import("resource://ontolingservices/SERVICE_stOntoLing.jsm",
		art_semanticturkey.ontoling);
Components.utils.import("resource://stmodules/StartST.jsm", art_semanticturkey);

art_semanticturkey.ontoling.chkST_started = function(){
	var stIsStarted = art_semanticturkey.ST_started.getStatus();
	if(stIsStarted=="true"){
		document.getElementById("menu_stOntoLing").disabled = false;
	}
};

art_semanticturkey.ontoling.checkOntoLingUp = function() {
	if (document.getElementById("STOntoLingstartMenuItem").disabled == false) {
		try {
			var responseXML = art_semanticturkey.ontoling.OLRequests.OntoLing
					.isOntoLingUp();
			if (responseXML != null)
				art_semanticturkey.ontoling.ontoLingUp_RESPONSE(responseXML);
		}
		catch (e) {
			alert(e.name + ": " + e.message);
		}
	} else if (art_semanticturkey.ontoling.stolModule.selRel == null) {
		document.getElementById("STOntoLingtoolsmenu").disabled = true;
	} else {
		document.getElementById("STOntoLingtoolsmenu").disabled = false;
	}
};

art_semanticturkey.ontoling.ontoLingUp_RESPONSE = function(responseElement) {
	var isUp = responseElement.getElementsByTagName("reply")[0]
			.getAttribute("status");
	if (isUp == "ok") {
		var status = responseElement.getElementsByTagName("infoStatus")[0].getAttribute("info");
		if(status == "ok"){
			document.getElementById("STOntoLingstartMenuItem").disabled = true;
			document.getElementById("STOntoLingManage").disabled = false;
			document.getElementById("STOntoLingtoolsmenu").disabled = false;
			document.getElementById("STOntoLingLoadResMenuItem").disabled = false;
			document.getElementById("STOntoLingReqLInst").disabled = false;
			// now we ask the server for the list of the interface of the linguistic
			// resources
			try {
				var responseXML = art_semanticturkey.ontoling.OLRequests.OntoLing
						.getLingInterfacesList();
				if (responseXML != null)
					art_semanticturkey.ontoling
							.getLingInterfacesList_RESPONSE(responseXML);
			} catch (e) {
				alert(e.name + ": " + e.message);
			}
		}
		else{
			alert("There was a problem during the statup of Ontoling, please check the file \nstontoling@art.uniroma2.it/extensions/properties/LWConfig.xml and then start stOntoLing again");
		}
		
	}
};

art_semanticturkey.ontoling.getLingInterfacesList_RESPONSE = function(
		responseElement) {
	var menupopuManage = document.getElementById("menupopup_stOntoLingManage");
	var lingInterfacesList = responseElement
			.getElementsByTagName('lingInterface');
	for ( var i = 0; i < lingInterfacesList.length; ++i) {
		var menuItemLingInt = document.createElement("menuitem");
		var interfaceId = lingInterfacesList[i].getAttribute("interfaceId");
		menuItemLingInt.setAttribute("label", interfaceId);
		menuItemLingInt.setAttribute("oncommand",
				"art_semanticturkey.ontoling.openWindowLingInstancesList('"
						+ interfaceId + "');");
		menupopuManage.appendChild(menuItemLingInt);
	}
};

art_semanticturkey.ontoling.openWindowLingInstancesList = function(interfaceId) {
	var parameters = new Object();
	parameters.interfaceId = interfaceId;
	parameters.changed = "false";
	window.openDialog("chrome://stontoling/content/modifyRes.xul", "_blank",
			"modal=yes,resizable,centerscreen", parameters);
	if (parameters.changed == "true") {
		art_semanticturkey.ontoling.getAllResource();
	}
};

art_semanticturkey.ontoling.startSTOntoLing = function() {
	try{
		var responseXML = art_semanticturkey.ontoling.OLRequests.OntoLing
				.startOntoLing();
		if (responseXML != null)
			art_semanticturkey.ontoling.ontoLingStarted_RESPONSE(responseXML);
	}
	catch (e) {
		alert(e.name + ": " + e.message);
	}
};

art_semanticturkey.ontoling.ontoLingStarted_RESPONSE = function(responseElement) {
	var valueStart = responseElement.getElementsByTagName("reply")[0]
			.getAttribute("status");
	if (valueStart == "ok") {
		document.getElementById("STOntoLingstartMenuItem").disabled = true;
		document.getElementById("STOntoLingManage").disabled = false;
		document.getElementById("STOntoLingtoolsmenu").disabled = false;
		document.getElementById("STOntoLingLoadResMenuItem").disabled = false;
		document.getElementById("STOntoLingReqLInst").disabled = false;
		// now we ask the server for the list of the interface of the linguistic
		// resources
		try {
			var responseXML = art_semanticturkey.ontoling.OLRequests.OntoLing
					.getLingInterfacesList();
			if (responseXML != null)
				art_semanticturkey.ontoling
						.getLingInterfacesList_RESPONSE(responseXML);
		} catch (e) {
			alert(e.name + ": " + e.message);
		}
		art_semanticturkey.ontoling.getAllLoadedRes();
	} else {
		alert("There where some problems in the OntoLing startup");
	}
};

art_semanticturkey.ontoling.getAllLoadedRes = function() {
	var parameters = new Object();
	window.openDialog("chrome://stontoling/content/chooseLoadedRes.xul",
			"_blank", "modal=yes,resizable,centerscreen", parameters);
};

art_semanticturkey.ontoling.loadOntoLing = function() {

	var url = "chrome://stontoling/content/STOntoLingTab.xul";

	var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"]
			.getService(Components.interfaces.nsIWindowMediator);
	var browserEnumerator = wm.getEnumerator("navigator:browser");

	var found = false;
	while (!found && browserEnumerator.hasMoreElements()) {
		var browserWin = browserEnumerator.getNext();
		var tabbrowser = browserWin.getBrowser();

		// Check each tab of this browser instance
		var numTabs = tabbrowser.browsers.length;
		for ( var index = 0; index < numTabs; index++) {
			var currentBrowser = tabbrowser.getBrowserAtIndex(index);

			if (url == currentBrowser.currentURI.spec) {
				// The URL is already opened. Select this tab.
				tabbrowser.selectedTab = tabbrowser.mTabs[index];

				// Focus *this* browser-window
				browserWin.focus();
				if (art_semanticturkey.ontoling.stolModule.tabWindow != null) {
					var lResId = art_semanticturkey.ontoling.stolModule.selRel;
					art_semanticturkey.ontoling.stolModule.tabWindow.art_semanticturkey.ontoling.loadTab();
					//if (lResId != art_semanticturkey.ontoling.stolModule.tabWindow.art_semanticturkey.ontoling.selResInTab)
					//	art_semanticturkey.ontoling.stolModule.tabWindow.art_semanticturkey.ontoling.loadTab();
				}
				found = true;
				break;
			}
		}
	}

	// Our URL isn't open. Open it now.
	if (!found) {
		var theTab = art_semanticturkey.ontoling.getMainWindow().gBrowser
				.addTab(url);
		theTab.label = "STOntoLing";
		art_semanticturkey.ontoling.getMainWindow().gBrowser.selectedTab = theTab;
	}

	// open the Semantic Turkey sidebar, whether it's closed or already open
	art_semanticturkey.ontoling.getMainWindow().toggleSidebar(
			"stOntologySidebar", true);
};

art_semanticturkey.ontoling.getAllResource = function() {
	var parameters = new Object();
	window.openDialog("chrome://stontoling/content/allRes.xul", "_blank",
			"modal=yes,resizable,centerscreen", parameters);
};

art_semanticturkey.ontoling.getMainWindow = function() {
	var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"]
			.getService(Components.interfaces.nsIWindowMediator);
	var mainWindow = wm.getMostRecentWindow("navigator:browser");

	return mainWindow;
};
