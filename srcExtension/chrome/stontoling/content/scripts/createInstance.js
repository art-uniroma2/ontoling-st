/*
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at http://www.mozilla.org/MPL/
 * 
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 * 
 * The Original Code is SemanticTurkey.
 * 
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 * 
 * SemanticTurkey was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata (ART) Current
 * information about SemanticTurkey can be obtained at
 * http://semanticturkey.uniroma2.it
 * 
 */
if (typeof art_semanticturkey == 'undefined') var art_semanticturkey = {};
if (!art_semanticturkey.ontoling) art_semanticturkey.ontoling = {};

Components.utils.import("resource://ontolingmodules/stOntoLingModule.jsm", art_semanticturkey.ontoling);
Components.utils.import("resource://ontolingservices/SERVICE_stOntoLing.jsm", art_semanticturkey.ontoling);

window.onload = function(){
	document.getElementById("ok").addEventListener(
			"click",art_semanticturkey.ontoling.onOk,true);
	document.getElementById("cancel").addEventListener(
			"click",art_semanticturkey.ontoling.onCancel,true);
	
	var vboxInstIdProp = document.getElementById("vboxInstanceIdProperties");
	var propList = window.arguments[0].propList;
	
	
	for(var i=0; i<propList.length; ++i){
		var propId = propList[i].propId;
		var propDescr = propList[i].propDescr;
		var hboxProp = document.createElement("hbox");
		vboxInstIdProp.appendChild(hboxProp);
		var labelPropId = document.createElement("label");
		labelPropId.setAttribute("value", propId+": ");
		hboxProp.appendChild(labelPropId);
		var textboxPropValue = document.createElement("textbox");
		textboxPropValue.setAttribute("flex",1);
		textboxPropValue.setAttribute("propId", propId);
		hboxProp.appendChild(textboxPropValue);
		var labelPropDesc = document.createElement("label");
		labelPropDesc.setAttribute("value", " ("+propDescr+")");
		hboxProp.appendChild(labelPropDesc);
	}
};


art_semanticturkey.ontoling.onOk = function(){
	var newInstanceId = document.getElementById("newInstanceId").value; //getAttribute("value");
	if(newInstanceId == ""){
		alert("Please input an id for the new instance");
		return;
	}
	var interfaceId = window.arguments[0].interfaceId;
	try {
		var responseXML = art_semanticturkey.ontoling.OLRequests.OntoLing.createLingInstance(interfaceId, newInstanceId);
		window.arguments[0].okChosen = "true";
		var textboxes = document.getElementsByTagName("textbox");
		for(var i=1; i<textboxes.length; ++i){
			var propValue = textboxes[i].value;
			var propId = textboxes[i].getAttribute("propId");
			try {
				art_semanticturkey.ontoling.OLRequests.OntoLing.updateLingInstancePropValue(
						newInstanceId,
						interfaceId,
						propId,
						propValue,
						"false");
			}
			catch (e) {
				alert(e.name + ": " + e.message);
			}
		}
		close();
	}
	catch (e) {
		alert(e.name + ": " + e.message);
	}
	
};

art_semanticturkey.ontoling.onCancel = function(){
	close();
};

