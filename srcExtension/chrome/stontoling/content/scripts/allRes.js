/*
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at http://www.mozilla.org/MPL/
 * 
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 * 
 * The Original Code is SemanticTurkey.
 * 
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 * 
 * SemanticTurkey was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata (ART) Current
 * information about SemanticTurkey can be obtained at
 * http://semanticturkey.uniroma2.it
 * 
 */
if (typeof art_semanticturkey == 'undefined') var art_semanticturkey = {};
if (!art_semanticturkey.ontoling) art_semanticturkey.ontoling = {};

Components.utils.import("resource://ontolingmodules/stOntoLingModule.jsm", art_semanticturkey.ontoling);
Components.utils.import("resource://ontolingservices/SERVICE_stOntoLing.jsm", art_semanticturkey.ontoling);

window.onload = function() {
	document.getElementById("ok").addEventListener("click",
			art_semanticturkey.ontoling.onAcceptAllRes, true);
	
	try {
		var responseXML = art_semanticturkey.ontoling.OLRequests.OntoLing.getPossibleLInt();
		if(responseXML != null)
			art_semanticturkey.ontoling.manageGetPossibleLInt_RESPONSE(responseXML);
	}
	catch (e) {
		alert(e.name + ": " + e.message);
	}
};

art_semanticturkey.ontoling.manageGetPossibleLInt_RESPONSE = function(responseElement){
	var allResList = responseElement.getElementsByTagName('possInst');
	var possResVBox = document.getElementById("possResVBox"); 
	
	for(var i=0; i < allResList.length; ++i){
		var possResHBoxTotal = document.createElement("hbox");
		var possResHBoxLabel = document.createElement("hbox");
		var possResHBoxButton = document.createElement("hbox");
		var resId = allResList[i].getAttribute("resId");
		var interfaceId = allResList[i].getAttribute("interfaceId");
		var labelRes = document.createElement("description");
		
		labelRes.setAttribute("value", interfaceId+" : "+resId);
		labelRes.setAttribute("resId", resId);
		possResHBoxLabel.appendChild(labelRes);
		
		var spacerNode = document.createElement("spacer");
		spacerNode.setAttribute("flex", "1");
		
		var loadButton = document.createElement("button");
		loadButton.setAttribute("label","LOAD");
		loadButton.setAttribute("id","loadButtonId"+i);
		loadButton.setAttribute("oncommand", "art_semanticturkey.ontoling.loadResButton('"+resId+"', 'loadButtonId"+i+"', 'unloadButtonId"+i+"');");
		possResHBoxButton.appendChild(loadButton);
		
		var unloadButton = document.createElement("button");
		unloadButton.setAttribute("label","UNLOAD");
		unloadButton.setAttribute("id","unloadButtonId"+i);
		unloadButton.setAttribute("oncommand", "art_semanticturkey.ontoling.unloadResButton('"+resId+"', 'loadButtonId"+i+"', 'unloadButtonId"+i+"');");
		possResHBoxButton.appendChild(unloadButton);
		
		
		if(allResList[i].getAttribute("loaded") == "true"){
			loadButton.setAttribute("hidden", true);
		}
		else{
			unloadButton.setAttribute("hidden", true);
		}
		
		possResHBoxTotal.appendChild(possResHBoxLabel);
		possResHBoxTotal.appendChild(spacerNode);
		possResHBoxTotal.appendChild(possResHBoxButton);
		possResVBox.appendChild(possResHBoxTotal);
	}
};

art_semanticturkey.ontoling.loadResButton = function(idRes, loadButtonId, unloadButtonId){
	art_semanticturkey.ontoling.disableAllResButton();
	art_semanticturkey.ontoling.loadLingRes(idRes, document.getElementById(loadButtonId), document.getElementById(unloadButtonId));
	art_semanticturkey.ontoling.enableAllResButton();
};

art_semanticturkey.ontoling.loadLingRes = function(resId, buttonLoad, buttonUnload){
	try{
		var responseXML = art_semanticturkey.ontoling.OLRequests.OntoLing.loadLingRes(resId);
		if(responseXML != null)
			art_semanticturkey.ontoling.loadedLingRes_RESPONSE(responseXML, buttonLoad, buttonUnload);
	}
	catch (e) {
		alert(e.name + ": " + e.message);
	}
};

art_semanticturkey.ontoling.loadedLingRes_RESPONSE = function(responseElement, buttonLoad, buttonUnload){
	if (responseElement.getElementsByTagName('reply')[0].getAttribute("status") == "ok"){// The resource was loaded
		buttonLoad.hidden = true;
		buttonUnload.hidden = false;
	}
	else{
		alert("There was a problem loading the selected linguistic resource");
		buttonLoad.hidden = false;
		buttonUnload.hidden = true;
	}
	
};

art_semanticturkey.ontoling.unloadResButton = function(idRes, loadButtonId, unloadButtonId){
	document.getElementById(loadButtonId).hidden = false;
	document.getElementById(unloadButtonId).hidden = true;
	art_semanticturkey.ontoling.disableAllResButton();
	art_semanticturkey.ontoling.unloadLingRes(idRes);
	art_semanticturkey.ontoling.enableAllResButton();
	
	var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"]
    	.getService(Components.interfaces.nsIWindowMediator);
    var mainWindow = wm.getMostRecentWindow("navigator:browser");
    var lResIdInOverlay = art_semanticturkey.ontoling.stolModule.selRel;
	if(lResIdInOverlay == idRes){
		art_semanticturkey.ontoling.stolModule.selRel = null;
		art_semanticturkey.ontoling.stolModule.langLRes = null;
		mainWindow.document.getElementById("STOntoLingSelResMenuItem").label = "null";
	}
	
};

art_semanticturkey.ontoling.unloadLingRes = function(resId){
	try{
		var responseXML = art_semanticturkey.ontoling.OLRequests.OntoLing.unloadLingRes(resId);
		if(responseXML != null)
			art_semanticturkey.ontoling.unloadedLingRes_RESPONSE(responseXML);
	}
	catch (e) {
		alert(e.name + ": " + e.message);
	}
};

art_semanticturkey.ontoling.unloadedLingRes_RESPONSE = function(responseElement){
	
};

art_semanticturkey.ontoling.enableAllResButton = function(){
	var allButtons = document.getElementsByTagName("button");
	for(var i=0; i<allButtons.legth; ++i){
		allButtons.disabled = false;
	}
};

art_semanticturkey.ontoling.disableAllResButton = function(){
	var allButtons = document.getElementsByTagName("button");
	for(var i=0; i<allButtons.legth; ++i){
		allButtons.disabled = true;
	}
};

art_semanticturkey.ontoling.onAcceptAllRes = function(){
	window.close();
	
	var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"]
    	.getService(Components.interfaces.nsIWindowMediator);
    var mainWindow = wm.getMostRecentWindow("navigator:browser");
	mainWindow.art_semanticturkey.ontoling.getAllLoadedRes();
};