/*
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at http://www.mozilla.org/MPL/
 * 
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 * 
 * The Original Code is SemanticTurkey.
 * 
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 * 
 * SemanticTurkey was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata (ART) Current
 * information about SemanticTurkey can be obtained at
 * http://semanticturkey.uniroma2.it
 * 
 */
//if (typeof art_semanticturkey == 'undefined') var art_semanticturkey = {};
//if (!art_semanticturkey.ontoling) art_semanticturkey.ontoling = {};

//Components.utils.import("resource://ontolingmodules/stOntoLingModule.jsm", art_semanticturkey.ontoling);

Components.utils.import("resource://stmodules/Logger.jsm", art_semanticturkey.ontoling);

Components.utils.import("resource://ontolingservices/SERVICE_stOntoLing.jsm",
		art_semanticturkey.ontoling);
Components.utils.import("resource://stservices/SERVICE_Cls.jsm",
		art_semanticturkey.ontoling);
Components.utils.import("resource://stservices/SERVICE_Property.jsm",
		art_semanticturkey.ontoling);
Components.utils.import("resource://stservices/SERVICE_ModifyName.jsm",
		art_semanticturkey.ontoling);

art_semanticturkey.ontoling.getMenuItemToBeAdded = function(id, labelText, selectedResURI,
		selectedRes, type, language, propType) {
	//alert("id = "+id+"\nlabelText = "+labelText+"\nselectedRes = "+selectedRes+"\ntype = "+type+"\npropType = "+propType); // da cancellare
	var GLOSS = "<GLOSS>";
	var ENTRY_TABLE = "<ENTRY_TABLE>"; // forse non serve
	var STRING_SEARCH = "<STRING_SEARCH>";
	var SENSE_ID = "<SENSE_ID>";
	var TERMS = "<TERMS>";
	var SLOT = "<SLOT>";
	var SELECTED_TERM = "<SELECTED_TERM>"; // indica l'elemento selezionato
	// dalla lista dei termini avuti dalla risorsa linguistica
	var SELECTED_RES = "<SELECTED_RES>"; // indica l'elemento selezionato
	// nell'ontologia, pu� essere o una  classe o una propriet�
	var LING_ENRICH = "<LING_ENRICH>";

	var menuItemNode;
	var tabWindow = art_semanticturkey.ontoling.getTabWindow();
	var labelToBeAdded;

	var selPropType=null; // used only with in the propery tree;
	if(type == "property"){
		selPropType = propType;
	}
	
	var gloss = tabWindow.document.getElementById("descriptionTextBox").value;
	var selectedTerm;
	if (tabWindow.document.getElementById("synonymsListBox").selectedIndex == -1)
		selectedTerm = "";
	else
		selectedTerm = tabWindow.document.getElementById("synonymsListBox").selectedItem
				.getElementsByTagName("label")[0].value;
	var senseId = tabWindow.document.getElementById("senseIDTextBox").value;

	var slot = "rdf:label"; // TODO SLOT

	labelToBeAdded = labelText;
	
	var containerObj = new Object();
	if (id == "menuSeparator") {
		menuItemNode = document.createElement("menuseparator");
	}
	
	else if (id == "searchLinguisticResource") {// SELECTED_RES
		menuItemNode = document.createElement("menuitem");
		if (selectedRes == "")
			menuItemNode.setAttribute("disabled", "true");
		else
			labelToBeAdded = labelToBeAdded.replace(SELECTED_RES, "'"
					+ selectedRes + "'");
		if (selectedRes != ""){
			containerObj.word = selectedRes;
			menuItemNode.containerObj = containerObj;
			menuItemNode.addEventListener("command", art_semanticturkey.ontoling.searchWord, true);
			//menuItemNode.setAttribute("oncommand",
			//		"art_semanticturkey.ontoling.searchWord('" + selectedRes
			//				+ "');");
		}
		menuItemNode.setAttribute("label", labelToBeAdded);
	}
	
	else if (id == "addTerm") { // SELECTED_TERM, SELECTED_RES e SLOT
		menuItemNode = document.createElement("menuitem");
		if (selectedTerm == "")
			menuItemNode.setAttribute("disabled", "true");
		else
			labelToBeAdded = labelToBeAdded.replace(SELECTED_TERM, "'"
					+ selectedTerm + "'");
		if (selectedRes == "")
			menuItemNode.setAttribute("disabled", "true");
		else
			labelToBeAdded = labelToBeAdded.replace(SELECTED_RES, "'"
					+ selectedRes + "'");
		menuItemNode.setAttribute("label", labelToBeAdded);
		if (slot == "")
			menuItemNode.setAttribute("disabled", "true");
		else
			labelToBeAdded = labelToBeAdded.replace(SLOT, "'" + slot + "'");
		if ((selectedTerm != "") && (selectedRes != "") && (slot != "")){
			//containerObj.resourceName = selectedRes;
			containerObj.resourceName = selectedResURI;
			containerObj.label = selectedTerm;
			containerObj.language = language;
			menuItemNode.containerObj = containerObj;
			menuItemNode.addEventListener("command", art_semanticturkey.ontoling.addLabel, true);
			//menuItemNode.setAttribute("oncommand",
			//		"art_semanticturkey.ontoling.addLabel('" + selectedRes
			//				+ "','" + selectedTerm + "');");  //TODO vedere se serve propType
		}
		menuItemNode.setAttribute("label", labelToBeAdded);
	} 
	
	else if (id == "addGloss") { // GLOSS e SELECTED_RES
		menuItemNode = document.createElement("menuitem");
		if (gloss == "")
			menuItemNode.setAttribute("disabled", "true");
		else
			labelToBeAdded = labelToBeAdded.replace(GLOSS, "'" + gloss + "'");
		if (selectedRes == "")
			menuItemNode.setAttribute("disabled", "true");
		else
			labelToBeAdded = labelToBeAdded.replace(SELECTED_RES, "'"
					+ selectedRes + "'");
		if ((gloss != "") && (selectedRes != "")){
			//containerObj.resourceName = selectedRes;
			containerObj.resourceName = selectedResURI;
			containerObj.description = gloss;
			containerObj.language = language;
			menuItemNode.containerObj = containerObj;
			menuItemNode.addEventListener("command", art_semanticturkey.ontoling.addDescription, true);
			//menuItemNode.setAttribute("oncommand", 
			//		"art_semanticturkey.ontoling.addDescription('" + selectedRes
			//		+ "','" + gloss + "');"); //TODO vedere se serve propType
		}
		menuItemNode.setAttribute("label", labelToBeAdded);
	} 
	
	else if (id == "addSenseIdToResource") {// SENSE_ID, SLOT e SELECTED_RES
		menuItemNode = document.createElement("menuitem");
		if (senseId == "")
			menuItemNode.setAttribute("disabled", "true");
		else
			labelToBeAdded = labelToBeAdded.replace(SENSE_ID, "'" + senseId
					+ "'");
		if (slot == "")
			menuItemNode.setAttribute("disabled", "true");
		else
			labelToBeAdded = labelToBeAdded.replace(SLOT, "'" + slot + "'");
		if (selectedRes == "")
			menuItemNode.setAttribute("disabled", "true");
		else
			labelToBeAdded = labelToBeAdded.replace(SELECTED_RES, "'"
					+ selectedRes + "'");
		if ((senseId != "") && (slot != "") && (selectedRes != "")){
			//containerObj.resourceName = selectedRes;
			containerObj.resourceName = selectedResURI;
			containerObj.label = senseId;
			containerObj.language = language;
			menuItemNode.containerObj = containerObj;
			menuItemNode.addEventListener("command", art_semanticturkey.ontoling.addLabel, true);
			//menuItemNode.setAttribute("oncommand", 
			//		"art_semanticturkey.ontoling.addLabel('" + selectedRes
			//		+ "','" + senseId + "');"); //TODO vedere se serve propType
		}
		menuItemNode.setAttribute("label", labelToBeAdded);
	}
	
	else if (id == "changeNameToSelectedTerm") { // SELECTED_RES e
		// SELECTED_TERM
		menuItemNode = document.createElement("menuitem");
		if (selectedRes == "")
			menuItemNode.setAttribute("disabled", "true");
		else
			labelToBeAdded = labelToBeAdded.replace(SELECTED_RES, "'"
					+ selectedRes + "'");
		if (selectedTerm == "")
			menuItemNode.setAttribute("disabled", "true");
		else
			labelToBeAdded = labelToBeAdded.replace(SELECTED_TERM, "'"
					+ selectedTerm + "'");
		if ((selectedRes != "") && (selectedTerm != "")){
			containerObj.newNameRes = selectedTerm;
			//containerObj.oldNameRes = selectedRes;
			containerObj.oldNameRes = selectedResURI;
			menuItemNode.containerObj = containerObj;
			menuItemNode.addEventListener("command", art_semanticturkey.ontoling.changeName, true);
			//menuItemNode.setAttribute("oncommand",
			//		"art_semanticturkey.ontoling.changeName('" + selectedTerm
			//				+ "','" + selectedRes + "')"); //TODO vedere se serve propType
		}
		menuItemNode.setAttribute("label", labelToBeAdded);
	}

	else if (id == "createSubResourceWithSelectedTermAsID") { // SELECTED_RES
		// and SELECTED_TERM
		menuItemNode = document.createElement("menuitem");
		if (selectedRes == "")
			menuItemNode.setAttribute("disabled", "true");
		else
			labelToBeAdded = labelToBeAdded.replace(SELECTED_RES, "'"
					+ selectedRes + "'");
		if (selectedTerm == "")
			menuItemNode.setAttribute("disabled", "true");
		else
			labelToBeAdded = labelToBeAdded.replace(SELECTED_TERM, "'"
					+ selectedTerm + "'");
		if ((selectedRes != "") && (selectedTerm != "")){
			containerObj.newResourceName = selectedTerm;
			//containerObj.parentResourceName = selectedRes;
			containerObj.parentResourceName = selectedResURI;
			containerObj.type = type;
			containerObj.propType = selPropType;
			menuItemNode.containerObj = containerObj;
			menuItemNode.addEventListener("command", art_semanticturkey.ontoling.addSubResource, true);
			//menuItemNode.setAttribute("oncommand",
			//		"art_semanticturkey.ontoling.addSubResource('"
			//				+ selectedTerm + "','" + selectedRes + "','" + type
			//				+ "','"+selPropType+"')");
		}
		menuItemNode.setAttribute("label", labelToBeAdded);
	} 
	
	else if (id == "addSubResourcesUsingSubConceptsFromLinguisticResources") {// SELECTED_RES
		// and SELECTED_TERM
		menuItemNode = document.createElement("menuitem");
		if (selectedRes == "")
			menuItemNode.setAttribute("disabled", "true");
		else
			labelToBeAdded = labelToBeAdded.replace(SELECTED_RES, "'"
					+ selectedRes + "'");
		if (selectedTerm == "")
			menuItemNode.setAttribute("disabled", "true");
		else
			labelToBeAdded = labelToBeAdded.replace(SELECTED_TERM, "'"
					+ selectedTerm + "'");
		if ((selectedRes != "") && (selectedTerm != "")){
			containerObj.semex = senseId;
			//containerObj.conceptName = selectedRes;
			containerObj.conceptName = selectedResURI;
			containerObj.type = type;
			containerObj.propType = selPropType;
			menuItemNode.containerObj = containerObj;
			menuItemNode.addEventListener("command", art_semanticturkey.ontoling.addSubConcepts, true);
			//menuItemNode.setAttribute("oncommand",
			//		"art_semanticturkey.ontoling.addSubConcepts('" + senseId
			//				+ "','" + selectedRes + "','"+ type + "','"+selPropType+"')");
		}
		menuItemNode.setAttribute("label", labelToBeAdded);
	} 
	
	else if (id == "callLinguisticEnrichment") {// LING_ENRICH
		menuItemNode = document.createElement("menuitem");
		labelToBeAdded = labelToBeAdded.replace(LING_ENRICH, "");
		//menuItemNode.setAttribute("oncommand", ""); // TODO
		menuItemNode.setAttribute("disabled", "true");
		menuItemNode.setAttribute("label", labelToBeAdded);
	}
	return menuItemNode;
};

/*
 * function tryComunicateWithWindows(){
 * 
 * var tabWindow = getTabWindow(); var mainWindow = getMainWindow();
 * 
 * if(tabWindow != null) tabWindow.searchLemma("computer"); else alert("il tab �
 * chiuso"); }
 */

art_semanticturkey.ontoling.getTabWindow = function() {
	return art_semanticturkey.ontoling.stolModule.tabWindow;
};

art_semanticturkey.ontoling.getMainWindow = function() {
	var mainWindow = null;

	var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"]
			.getService(Components.interfaces.nsIWindowMediator);
	mainWindow = wm.getMostRecentWindow("navigator:browser");

	return mainWindow;
};

/** ******************************************************************************** */
// These are the function called by the menuitem in the popupMenu
art_semanticturkey.ontoling.searchWord = function(event) {
	var word = event.target.containerObj.word;
	art_semanticturkey.ontoling.getMainWindow().art_semanticturkey.ontoling
			.loadOntoLing();
	art_semanticturkey.ontoling.getTabWindow().art_semanticturkey.ontoling
			.searchInputWord(word);
};


art_semanticturkey.ontoling.changeName = function(event) {
	var isNotModifiable = art_semanticturkey.ontoling.getSelectedTreeCell()
			.getAttribute("deleteForbidden");
	if (isNotModifiable == "false") {
		try {
			var newNameRes = event.target.containerObj.newNameRes;
			var oldNameRes = event.target.containerObj.oldNameRes;
			var responseXML = art_semanticturkey.ontoling.STRequests.ModifyName
					.renameResource(newNameRes,
							oldNameRes);
			art_semanticturkey.renameResource_RESPONSE(responseXML);
		}
		catch (e) {
			alert(e.name + ": " + e.message);
		}
	} else
		alert("You cannot modify this resource, it's a system resource!");
};


art_semanticturkey.ontoling.addSubResource = function(event) {
	var responseXML;
	try {
		var newResourceName = event.target.containerObj.newResourceName;
		var parentResourceName = event.target.containerObj.parentResourceName;
		var type = event.target.containerObj.type;
		var propType = event.target.containerObj.propType;
		if(type == "class")
			responseXML = art_semanticturkey.ontoling.STRequests.Cls.addSubClass(
				newResourceName,
				parentResourceName);
		else { //type == "property"
			responseXML = art_semanticturkey.ontoling.STRequests.Property.addSubProperty(
					newResourceName,
					propType,
					parentResourceName);
		}
		if (type == "class")
			art_semanticturkey.addClass_RESPONSE(responseXML);
		else { //type == "property"
			art_semanticturkey.addProperty_RESPONSE(responseXML);
		}
	}
	catch (e) {
		alert(e.name + ": " + e.message);
	}
};


art_semanticturkey.ontoling.addSubConcepts = function(event) {
	var responseArray;
	try{
		var semex = event.target.containerObj.semex;
		var conceptName = event.target.containerObj.conceptName;
		var type = event.target.containerObj.type;
		var propType = event.target.containerObj.propType;
		if (type == "class")
			responseArray = art_semanticturkey.ontoling.OLRequests.OntoLing
					.addSubConcepts(semex,
							conceptName, 
							type);
		else //type == "property"
			responseArray = art_semanticturkey.ontoling.OLRequests.OntoLing
					.addSubConcepts(semex,
							conceptName, propType);
		if (responseArray != null)
			if(type == "class")
				art_semanticturkey.ontoling.addSubConcepts_RESPONSE(responseArray, type);
			else { //type == "property"
				art_semanticturkey.ontoling.addSubConcepts_RESPONSE(responseArray, type, propType);
		}
	}
	catch (e) {
		alert(e.name + ": " + e.message);
	}
};




art_semanticturkey.ontoling.addLabel = function(event) {
	try{
		var resourceName = event.target.containerObj.resourceName;
		var label = event.target.containerObj.label;
		var language = event.target.containerObj.language;
		var responseXML = art_semanticturkey.ontoling.STRequests.Property.createAndAddPropValue(
				resourceName,
				"rdfs:label",
				label,
				"owl:Thing",
				"plainLiteral",
				language);
	}
	catch (e) {
		alert(e.name + ": " + e.message);
	}
};

art_semanticturkey.ontoling.addDescription = function(event) {
	try{
		var resourceName = event.target.containerObj.resourceName;
		var description = event.target.containerObj.description;
		var language = event.target.containerObj.language;
		var responseXML = art_semanticturkey.ontoling.STRequests.Property.createAndAddPropValue(
				resourceName,
				"rdfs:comment",
				description,
				"owl:Thing",
				"plainLiteral",
				language);
	}
	catch (e) {
		alert(e.name + ": " + e.message);
	}
};



art_semanticturkey.ontoling.addSubConcepts_RESPONSE = function(responseArray, type, propType){
	var tree;
	var childList;
	if(type == "class") {
		tree = document.getElementById("classesTree");
		var childList = tree.getElementsByTagName("treeitem");
		for ( var i = 0; i < childList.length; i++) {
			for(var k=0; k<responseArray["subConcepts"].length; ++k){
				art_semanticturkey.checkAndCreate(responseArray["subConcepts"][k], childList[i],
						responseArray["concept"]); // This function is in classEvents.js
			}
		}
	}
	else { // type == "property"
		tree = document.getElementById("propertiesTree");
		childList = tree.getElementsByTagName("treeitem");
		
		for(var i=0; i<childList.length; i++){
			var propNodeName = childList[i].getElementsByTagName("treecell")[0].getAttribute("label");
			if(propNodeName == superResourceName){
				var treechildren = childList[i].getElementsByTagName('treechildren')[0];
				
				if (treechildren == null) {
					treechildren = document.createElement("treechildren");
					childList[i].appendChild(treechildren);
				}
				
				for(var k=0; k<addedResources.length; ++k){
					var addedPropertyName = addedResources[k].getAttribute("name");
					var tr = document.createElement("treerow");
					var tc = document.createElement("treecell");
					var ti = document.createElement("treeitem");
					tc.setAttribute("label", addedPropertyName);
					tc.setAttribute("deleteForbidden", "false");
					tc.setAttribute("numInst", "0");
					tc.setAttribute("isRootNode", false);
					tc.setAttribute("properties", propType.substring(4));
					tc.setAttribute("type", propType);
					tr.setAttribute("properties", propType.substring(4));
					tr.appendChild(tc);
					ti.setAttribute('container', 'false');
					ti.setAttribute('open', 'false');
					ti.setAttribute("propertyName", addedPropertyName); 
					ti.appendChild(tr);
					
					if (childList[i].getAttribute('container') == "false") {
						childList[i].setAttribute('container', 'true');
						childList[i].setAttribute('open', 'true');
					} else if (childList[i].getAttribute('open') == "false") {
						childList[i].setAttribute('open', 'true');
					}
					treechildren.appendChild(ti);
				}
			}
		}
	}
	var message;
	var types = art_semanticturkey.ontoling.plural(type, responseArray.length);
	message = "the following "+ types +" were added:\n";
	for(var k=0; k<responseArray["subConcepts"].length; ++k){
		message += "\t"+responseArray["subConcepts"][k].getShow()+"\n";
	}
	/*var existingResources = responseElement.getElementsByTagName("existingResources")[0].getElementsByTagName("resource");
	types = art_semanticturkey.ontoling.plural(type, existingResources.length);
	message +="\nthe following "+ types +" were not added beacause there exist resource(s) with the same id:\n";
	for(var k=0; k<existingResources.length; ++k){
		message += "\t"+existingResources[k].getAttribute("name")+"\n";
	}*/
	var parameters = new Object();
	parameters.message = message;
	window.openDialog("chrome://stontoling/content/resourcesAdded.xul","_blank","modal=yes,resizable,centerscreen", parameters);
	//alert(message);
};

art_semanticturkey.ontoling.plural = function(type, number){
	if(number > 1){
		if(type == "class")
			return "classes";
		return "properties";
	}
	return type;
};