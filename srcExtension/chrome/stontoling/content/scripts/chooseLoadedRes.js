/*
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at http://www.mozilla.org/MPL/
 * 
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 * 
 * The Original Code is SemanticTurkey.
 * 
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 * 
 * SemanticTurkey was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata (ART) Current
 * information about SemanticTurkey can be obtained at
 * http://semanticturkey.uniroma2.it
 * 
 */
if (typeof art_semanticturkey == 'undefined') var art_semanticturkey = {};
if (!art_semanticturkey.ontoling) art_semanticturkey.ontoling = {};

Components.utils.import("resource://ontolingmodules/stOntoLingModule.jsm", art_semanticturkey.ontoling);
Components.utils.import("resource://ontolingservices/SERVICE_stOntoLing.jsm", art_semanticturkey.ontoling);

window.onload = function() {
	document.getElementById("resChosen").addEventListener("click",
			art_semanticturkey.ontoling.onAcceptLoadedRes, true);
	/*document.getElementById("cancel").addEventListener("click",
			art_semanticturkey.ontoling.onClose, true);*/
	
	try {
		var responseXML = art_semanticturkey.ontoling.OLRequests.OntoLing.getLoadedLInt();
		if(responseXML != null)
			art_semanticturkey.ontoling.manageGetLoadedLInt_RESPONSE(responseXML);
	}
	catch (e) {
		alert(e.name + ": " + e.message);
	}
};

art_semanticturkey.ontoling.manageGetLoadedLInt_RESPONSE = function(responseElement){
	var loadResList = responseElement.getElementsByTagName('loadedRes');
	var radioGroupSelRes = document.getElementById("radioGroupSelRes"); 
	for(var i=0; i < loadResList.length; ++i){
		var radioSelRes = document.createElement('radio');
		var resId = loadResList[i].getAttribute("resId");
		var langLRes = loadResList[i].getAttribute("langLRes");
		var interfaceId = loadResList[i].getAttribute("interfaceId");
		radioSelRes.setAttribute("label", interfaceId+" : "+resId);
		radioSelRes.setAttribute("resId", resId);
		radioSelRes.setAttribute("langLRes", langLRes);
		radioSelRes.setAttribute("interfaceId", interfaceId);
		if(i==0){
			radioSelRes.setAttribute("selected", true);
		}
		radioGroupSelRes.appendChild(radioSelRes);
	}
};

art_semanticturkey.ontoling.onAcceptLoadedRes = function(){
	var radioGroupSelRes = document.getElementById("radioGroupSelRes");
	var selectedItem = radioGroupSelRes.selectedItem;
	var resId = selectedItem.getAttribute("resId");
	var languageLRes = selectedItem.getAttribute("langLRes");
	
	var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"]
    	.getService(Components.interfaces.nsIWindowMediator);
   	var mainWindow = wm.getMostRecentWindow("navigator:browser");
   
   	art_semanticturkey.ontoling.stolModule.selRel = resId;
   	art_semanticturkey.ontoling.stolModule.langLRes = languageLRes;
	mainWindow.document.getElementById("STOntoLingSelResMenuItem").label = resId;   
   
   	//loadOntoLing();
	close();
	mainWindow.art_semanticturkey.ontoling.loadOntoLing(); 
};

art_semanticturkey.ontoling.onClose = function() {
	close();
};