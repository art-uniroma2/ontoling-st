/*
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at http://www.mozilla.org/MPL/
 * 
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 * 
 * The Original Code is SemanticTurkey.
 * 
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 * 
 * SemanticTurkey was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata (ART) Current
 * information about SemanticTurkey can be obtained at
 * http://semanticturkey.uniroma2.it
 * 
 */
if (typeof art_semanticturkey == 'undefined') var art_semanticturkey = {};
if (!art_semanticturkey.ontoling) art_semanticturkey.ontoling = {};
if (!art_semanticturkey.ontoling.selResInTab) art_semanticturkey.ontoling.selResInTab = null;


Components.utils.import("resource://ontolingmodules/stOntoLingModule.jsm", art_semanticturkey.ontoling);
Components.utils.import("resource://ontolingservices/SERVICE_stOntoLing.jsm", art_semanticturkey.ontoling);


window.onload = function(){
		art_semanticturkey.ontoling.loadTab();
};


window.onunload = function(){
	art_semanticturkey.ontoling.unloadTab();
};

art_semanticturkey.ontoling.loadTab = function(){
  	var lResId = art_semanticturkey.ontoling.stolModule.selRel;
  
	if(lResId != null){
		art_semanticturkey.ontoling.selResInTab = lResId;
		art_semanticturkey.ontoling.selectLoadedRes(lResId);
	}
	
	art_semanticturkey.ontoling.stolModule.tabWindow = window;
	
	document.getElementById("synonymsBox").addEventListener("dblclick", art_semanticturkey.ontoling.eventoDblClickgroupbox, false);
	document.getElementById("semrelmenulist").addEventListener("select", art_semanticturkey.ontoling.semanticRelationinTab, false);
	document.getElementById("lexrelmenulist").addEventListener("select", art_semanticturkey.ontoling.lexicalRelationinTab, false);
	document.getElementById("searchTextBox").addEventListener("command", art_semanticturkey.ontoling.searchWordInTab, false);
	document.getElementById("searchButton").addEventListener("command", art_semanticturkey.ontoling.searchWordInTab, false);
	document.getElementById("resultTree").addEventListener("select", art_semanticturkey.ontoling.populateBoxes, false);
	
};

art_semanticturkey.ontoling.unloadTab = function(){
    art_semanticturkey.ontoling.stolModule.tabWindow = null;
    art_semanticturkey.ontoling.stolModule.ctxClassMenu.clear();
    art_semanticturkey.ontoling.stolModule.ctxPropertyMenu.clear();
};

art_semanticturkey.ontoling.searchWordInTab = function(){
	var word;
	word = document.getElementById("searchTextBox").value;
	art_semanticturkey.ontoling.searchInputWord(word);
};


art_semanticturkey.ontoling.lexicalRelationinTab = function(){
	var lResIdInOverlay = art_semanticturkey.ontoling.stolModule.selRel;
	if(lResIdInOverlay == null){
		alert("No resource is selected, please use the proper menu to select a resource");
		return;
	}
	else if(lResIdInOverlay != art_semanticturkey.ontoling.selResInTab){
		alert("The loadedRes has changed from "+art_semanticturkey.ontoling.selResInTab+" to "+lResIdInOverlay+", so the tab will change accordingly");
		art_semanticturkey.ontoling.loadTab();
		return;
	}
	
	var pos = document.getElementById("synonymsListBox").selectedIndex;
	if(pos == -1)
		return;
	var listItemSelectedNode = document.getElementById("synonymsListBox").getItemAtIndex(pos);
	var labelNode = listItemSelectedNode.getElementsByTagName("label")[0];
	var lemma = labelNode.getAttribute("value");
	
	var resultTreeNode = document.getElementById("resultTree");
	if(resultTreeNode.currentIndex == -1)
		return
	var selectedTreeItemNode = resultTreeNode.contentView.getItemAtIndex(resultTreeNode.currentIndex);
	var semex = selectedTreeItemNode.getAttribute("sense");
	
	var lexrelmenulistNode = document.getElementById("lexrelmenulist");
	var lexselectedItem = lexrelmenulistNode.selectedItem;
	var lexRel = lexselectedItem.label;
	lexrelmenulistNode.selectedIndex = -1;
	
	art_semanticturkey.ontoling.useLexicalRelation(lemma, semex, lexRel);
};


art_semanticturkey.ontoling.semanticRelationinTab = function(){
	if(document.getElementById("semrelmenulist").selectedIndex == -1)
		return;
	var lResIdInOverlay = art_semanticturkey.ontoling.stolModule.selRel;
	if(lResIdInOverlay == null){
		alert("No resource is selected, please use the proper menu to select a resource");
		return;
	}
	else if(lResIdInOverlay != art_semanticturkey.ontoling.selResInTab){
		alert("The loadedRes has changed from "+art_semanticturkey.ontoling.selResInTab+" to "+lResIdInOverlay+", so the tab will change accordingly");
		art_semanticturkey.ontoling.loadTab();
		return;
	}
	
	var resultTreeNode = document.getElementById("resultTree");
	if (resultTreeNode.currentIndex == -1)
		return;
	var selectedTreeItemNode = resultTreeNode.contentView.getItemAtIndex(resultTreeNode.currentIndex);
	var semex = selectedTreeItemNode.getAttribute("sense");
	
	var semrelmenulistNode = document.getElementById("semrelmenulist");
	var semselectedItemNode = semrelmenulistNode.selectedItem;
	var semRel = semselectedItemNode.label;
	art_semanticturkey.ontoling.useSemanticRelation(semex, semRel);
	semrelmenulistNode.selectedIndex = -1;
};



/**********************************************************************************/


// This function is called only from loadTab()
art_semanticturkey.ontoling.selectLoadedRes = function(resId){
	try{
		var responseXML = art_semanticturkey.ontoling.OLRequests.OntoLing.selectLingRes(resId);
		if(responseXML != null)
			art_semanticturkey.ontoling.populateTab_RESPONSE(responseXML); 
	}
	catch (e) {
		alert(e.name + ": " + e.message);
	}
};

art_semanticturkey.ontoling.searchInputWord = function(word){
	var lResIdInOverlay = art_semanticturkey.ontoling.stolModule.selRel;
	if(lResIdInOverlay == null){
		alert("No resource is selected, please use the proper menu to select a resource");
		return;
	}
	else if(art_semanticturkey.ontoling.selResInTab == null){
		alert("When the tab was opened no Resource was yet selected, so now "+lResIdInOverlay+" will be selected and the tab will change accordingly");
		art_semanticturkey.ontoling.loadTab();
		return;
	}
	else if(lResIdInOverlay != art_semanticturkey.ontoling.selResInTab){
		alert("The loadedRes has changed from "+art_semanticturkey.ontoling.selResInTab+" to "+lResIdInOverlay+", so the tab will change accordingly");
		art_semanticturkey.ontoling.loadTab();
		return;
	}
	
	var searchFilters = new Array();
	
	document.getElementById("searchTextBox").value = word;
	var srchStr = document.getElementById("srchstrmenulist").selectedItem.label; 
	var srchFilterBox = document.getElementById("filterBox");
	var srchFilterListNode = srchFilterBox.getElementsByTagName("checkbox");
	
	for(var i = 0; i<srchFilterListNode.length; ++i){
		if(srchFilterListNode[i].checked)
			searchFilters[i] = srchFilterListNode[i].label;
	}
	art_semanticturkey.ontoling.searchWord(word, srchStr, searchFilters);
};

art_semanticturkey.ontoling.searchWord = function(word, srchStr, searchFilters){
	// first of all clean all the boxes and label in the tab
	document.getElementById("searchedTextBox").value = "";
	document.getElementById("senseIDTextBox").value ="";
	document.getElementById("descriptionTextBox").value = "";
	var synonymsListBoxNode = document.getElementById("synonymsListBox");
	while(synonymsListBoxNode.childNodes[0]){
		synonymsListBoxNode.removeChild(synonymsListBoxNode.childNodes[0]);
	}
	var  resultTreeChildrenNode = document.getElementById("resultTreeChildren");
	while(resultTreeChildrenNode.childNodes[0]){  
		resultTreeChildrenNode.removeChild(resultTreeChildrenNode.childNodes[0]);
	}
	try{
		var responseXML = art_semanticturkey.ontoling.OLRequests.OntoLing.searchWord(word, srchStr, searchFilters);
		if(responseXML != null)
			art_semanticturkey.ontoling.searchWord_RESPONSE(responseXML);
	}
	catch (e) {
		alert(e.name + ": " + e.message);
	}
};


art_semanticturkey.ontoling.populateTab_RESPONSE = function(responseXMLContent) {
	var semRelList = responseXMLContent.getElementsByTagName('semRelId');
	var lexRelList = responseXMLContent.getElementsByTagName('lexRelId');
	var srchStrList = responseXMLContent.getElementsByTagName('srchStrId'); 
	var srchFilterList = responseXMLContent.getElementsByTagName('srchFilterId');
	var ctxMenuClassItemList = responseXMLContent.getElementsByTagName('ctxMenuClassItem');
	var ctxMenuPropertyItemList = responseXMLContent.getElementsByTagName('ctxMenuPropertyItem');
	
	
	// first of all clean all the boxes and label in the tab
	document.getElementById("searchTextBox").value = "";
	document.getElementById("searchedTextBox").value = "";
	document.getElementById("senseIDTextBox").value ="";
	document.getElementById("descriptionTextBox").value = "";
	var synonymsListBoxNode = document.getElementById("synonymsListBox");
	while(synonymsListBoxNode.childNodes[0]){
		synonymsListBoxNode.removeChild(synonymsListBoxNode.childNodes[0]);
	}
	var  resultTreeChildrenNode = document.getElementById("resultTreeChildren");
	while(resultTreeChildrenNode.childNodes[0]){  
		resultTreeChildrenNode.removeChild(resultTreeChildrenNode.childNodes[0]);
	}
	
	var semRelMenuPopup = document.getElementById("semrelmenupopup");
	var lexRelMenuPopup = document.getElementById("lexrelmenupopup");
	var srchStrMenuPopup = document.getElementById("srchstrmenupopup");
	var filterBox = document.getElementById("filterBox");
	
	while(semRelMenuPopup.childNodes[0]){  
		semRelMenuPopup.removeChild(semRelMenuPopup.childNodes[0]);
	}
	if(semRelList.length == 0)
		document.getElementById("semrelmenulist").disabled = true;
	else{
		document.getElementById("semrelmenulist").disabled = false;
		for(var i = 0; i<semRelList.length; ++i){
			var semRelMenuItem = document.createElement("menuitem");
			semRelMenuItem.setAttribute("label", semRelList[i].getAttribute("semRelId"));
			semRelMenuPopup.appendChild(semRelMenuItem);
		}
	}
	
	while(lexRelMenuPopup.childNodes[0]){  
		lexRelMenuPopup.removeChild(lexRelMenuPopup.childNodes[0]);
	}
	if(lexRelList.length == 0)
		document.getElementById("lexrelmenulist").disabled = true;
	else{
		document.getElementById("lexrelmenulist").disabled = false;
		for(var i = 0; i<lexRelList.length; ++i){
			var lexRelMenuItem = document.createElement("menuitem");
			lexRelMenuItem.setAttribute("label", lexRelList[i].getAttribute("lexRelId"));
			lexRelMenuPopup.appendChild(lexRelMenuItem);
		}
	}
	
	while(srchStrMenuPopup.childNodes[0]){  
		srchStrMenuPopup.removeChild(srchStrMenuPopup.childNodes[0]);
	}
	for(var i = 0; i<srchStrList.length; ++i){
		var srchStrMenuItem = document.createElement("menuitem");
		srchStrMenuItem.setAttribute("label", srchStrList[i].getAttribute("srchStrId"));
		srchStrMenuPopup.appendChild(srchStrMenuItem);
		if(i == 0){
			document.getElementById("srchstrmenulist").selectedItem = srchStrMenuItem;
		}
	}
	
	while(filterBox.childNodes[0]){  
		filterBox.removeChild(filterBox.childNodes[0]);
	}
	for(var i = 0; i<srchFilterList.length; ++i){
		var srchFilterCheckBox = document.createElement("checkbox");
		srchFilterCheckBox.setAttribute("label", srchFilterList [i].getAttribute("srchFilterId"));
		srchFilterCheckBox.setAttribute("checked", srchFilterList[i].getAttribute("active"));
		filterBox.appendChild(srchFilterCheckBox);
	}
	art_semanticturkey.ontoling.stolModule.ctxClassMenu.clear();
    for(var i = 0; i<ctxMenuClassItemList.length; ++i){
    	art_semanticturkey.ontoling.stolModule.ctxClassMenu.addLabelAndId(ctxMenuClassItemList[i].getAttribute("label"), 
    		ctxMenuClassItemList[i].getAttribute("name"));
    }
    
    art_semanticturkey.ontoling.stolModule.ctxPropertyMenu.clear();
    for(var i = 0; i<ctxMenuPropertyItemList.length; ++i){
    	art_semanticturkey.ontoling.stolModule.ctxPropertyMenu.addLabelAndId(ctxMenuPropertyItemList[i].getAttribute("label"), 
    			ctxMenuPropertyItemList[i].getAttribute("name"));
    }
};


art_semanticturkey.ontoling.searchWord_RESPONSE = function(responseXMLContent){
	var searchedWordList = responseXMLContent.getElementsByTagName("searchWord");
	if(searchedWordList.length == 0){
		return;
	}
	else if(searchedWordList.length == 1){
		art_semanticturkey.ontoling.searchLemma(searchedWordList[0].getAttribute("value"));
	}
	else{
		var parameters = new Object();
		parameters.lemmaList = searchedWordList;
		parameters.tabWindow = window;
		window.openDialog("chrome://stontoling/content/chooseLemma.xul","_blank","modal=yes,resizable,centerscreen", parameters);
	}
};


art_semanticturkey.ontoling.searchLemma = function(lemma){
	var lResIdInOverlay = art_semanticturkey.ontoling.stolModule.selRel;
	
	if(lResIdInOverlay == null){
		alert("No resource is selected, please use the proper menu to select a resource");
		return;
	}
	else if(art_semanticturkey.ontoling.selResInTab == null){
		alert("When the tab was opened no Resource was yet selected, so now "+lResIdInOverlay+" will be selected and the tab will change accordingly");
		art_semanticturkey.ontoling.loadTab();
		return;
	}
	else if(lResIdInOverlay != art_semanticturkey.ontoling.selResInTab){
		alert("The loadedRes has changed from "+art_semanticturkey.ontoling.selResInTab+" to "+lResIdInOverlay+", so the tab will change accordingly");
		art_semanticturkey.ontoling.loadTab();
		return;
	}
	try{
		var responseXML = art_semanticturkey.ontoling.OLRequests.OntoLing.searchLemma(lemma);
		if(responseXML != null)
			art_semanticturkey.ontoling.searchLemma_RESPONSE(responseXML);
	}
	catch (e) {
		alert(e.name + ": " + e.message);
	}
};

art_semanticturkey.ontoling.searchLemma_RESPONSE = function(responseXMLContent){
	var lemma = responseXMLContent.getElementsByTagName("searchLemma");
	art_semanticturkey.ontoling.populateResultTable(lemma, responseXMLContent, false);
};

art_semanticturkey.ontoling.populateResultTableRelation_RESPONSE = function(responseXMLContent){
	art_semanticturkey.ontoling.populateResultTable(null, responseXMLContent, true);
};

art_semanticturkey.ontoling.populateResultTable = function(lemma, responseXMLContent, isFromRelation) {
	var tableRowList = responseXMLContent.getElementsByTagName("tableRow");
	if(tableRowList.length == 0){
		return
	}
	
	if(lemma != null)	
		document.getElementById("searchedTextBox").value = lemma[0].getAttribute("lemma");
	
	var  resultTreeChildrenNode = document.getElementById("resultTreeChildren");
	while(resultTreeChildrenNode.childNodes[0]){  
		resultTreeChildrenNode.removeChild(resultTreeChildrenNode.childNodes[0]);
	}
	
	
	if((isFromRelation == false) || (tableRowList.length > 0)){
		document.getElementById("senseIDTextBox").value ="";
		document.getElementById("descriptionTextBox").value = "";
		var synonymsListBoxNode = document.getElementById("synonymsListBox");
		while(synonymsListBoxNode.childNodes[0]){
			synonymsListBoxNode.removeChild(synonymsListBoxNode.childNodes[0]);
		}
	}
	
	for(var i = 0; i<tableRowList.length; ++i){
		var treeitemNode = document.createElement("treeitem");
		var treerowNode = document.createElement("treerow");
		
		var treecellId = document.createElement("treecell");
		treecellId.setAttribute("label", tableRowList[i].getElementsByTagName("tableCellID")[0].getAttribute("value"));
		treeitemNode.setAttribute("sense", tableRowList[i].getElementsByTagName("tableCellID")[0].getAttribute("value"));
		
		var treecellWord = document.createElement("treecell");
		treecellWord.setAttribute("label", tableRowList[i].getElementsByTagName("tableCellWord")[0].getAttribute("value"));
		treeitemNode.setAttribute("word", tableRowList[i].getElementsByTagName("tableCellWord")[0].getAttribute("value"));
		
		var treecellDesc = document.createElement("treecell");
		treecellDesc.setAttribute("label", tableRowList[i].getElementsByTagName("tableCellDesc")[0].getAttribute("value"));
		treeitemNode.setAttribute("description", tableRowList[i].getElementsByTagName("tableCellDesc")[0].getAttribute("value"));
		
		var synonymsList = tableRowList[i].getElementsByTagName("synonym");
		treeitemNode.setAttribute("synonymsNum", synonymsList.length);
		for(var k = 0; k<synonymsList.length; ++k){
			treeitemNode.setAttribute("synonym"+k, synonymsList[k].getAttribute("value"));
		}
		
		treerowNode.appendChild(treecellId);
		treerowNode.appendChild(treecellWord);
		treerowNode.appendChild(treecellDesc);
		treeitemNode.appendChild(treerowNode);
		
		resultTreeChildrenNode.appendChild(treeitemNode);
	}
};

art_semanticturkey.ontoling.populateBoxes = function(){
	var senseIDTextBoxNode = document.getElementById("senseIDTextBox");
	var descriptionTextBoxNode = document.getElementById("descriptionTextBox");
	
	var resultTreeNode = document.getElementById("resultTree");
	var selectedTreeItemNode = resultTreeNode.contentView.getItemAtIndex(resultTreeNode.currentIndex);
	
	var textSenseID = selectedTreeItemNode.getAttribute("sense");
	var textWord = selectedTreeItemNode.getAttribute("word");
	var textDescription = selectedTreeItemNode.getAttribute("description");
	
	senseIDTextBoxNode.value = textSenseID;
	descriptionTextBoxNode.value = textDescription;
	
	var synonymsListBoxNode = document.getElementById("synonymsListBox");
	while(synonymsListBoxNode.childNodes[0]){
		synonymsListBoxNode.removeChild(synonymsListBoxNode.childNodes[0]);
	}
	for(var i = 0; i<selectedTreeItemNode.getAttribute("synonymsNum"); ++i){
		var newLisItem = document.createElement("listitem");
		newLisItem.setAttribute("id", "synonymsListItem_"+i);
		var newLabel= document.createElement("label");
		newLabel.setAttribute("value",selectedTreeItemNode.getAttribute("synonym"+i));
		newLisItem.appendChild(newLabel);
		synonymsListBoxNode.appendChild(newLisItem);
		if(i == 0)
			synonymsListBoxNode.selectedItem = newLisItem;
	}
};

art_semanticturkey.ontoling.eventoDblClickgroupbox = function(){
	var pos = document.getElementById("synonymsListBox").selectedIndex;
	if(pos == -1)
		return;
	var selectedListItemNode = document.getElementById("synonymsListBox").selectedItem;
	var synonymSelected = selectedListItemNode.getElementsByTagName("label")[0].value;
	art_semanticturkey.ontoling.searchLemma(synonymSelected);
};


art_semanticturkey.ontoling.useLexicalRelation = function(lemma, semex, lexRel){
	try{
		var responseXML = art_semanticturkey.ontoling.OLRequests.OntoLing.lexRel(lemma, 
				semex, lexRel);
		if(responseXML != null)
			art_semanticturkey.ontoling.populateResultTableRelation_RESPONSE(responseXML);
	}
	catch (e) {
		alert(e.name + ": " + e.message);
	}
};

art_semanticturkey.ontoling.useSemanticRelation = function(semex, semRel){
	try{
		var responseXML = art_semanticturkey.ontoling.OLRequests.OntoLing.semRel(semex, semRel);
		if(responseXML != null)
			art_semanticturkey.ontoling.populateResultTableRelation_RESPONSE(responseXML);
	}
	catch (e) {
		alert(e.name + ": " + e.message);
	}
};
