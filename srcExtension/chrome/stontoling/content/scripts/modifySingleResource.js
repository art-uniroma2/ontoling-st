/*
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at http://www.mozilla.org/MPL/
 * 
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 * 
 * The Original Code is SemanticTurkey.
 * 
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 * 
 * SemanticTurkey was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata (ART) Current
 * information about SemanticTurkey can be obtained at
 * http://semanticturkey.uniroma2.it
 * 
 */
if (typeof art_semanticturkey == 'undefined') var art_semanticturkey = {};
if (!art_semanticturkey.ontoling) art_semanticturkey.ontoling = {};

Components.utils.import("resource://ontolingmodules/stOntoLingModule.jsm", art_semanticturkey.ontoling);
Components.utils.import("resource://ontolingservices/SERVICE_stOntoLing.jsm", art_semanticturkey.ontoling);


window.onload = function(){
	document.getElementById("ok").addEventListener("click",
			art_semanticturkey.ontoling.onOk, true);
	document.getElementById("cancel").addEventListener("click",
			art_semanticturkey.ontoling.onCancel, true);
	
	var groupboxModifySingelRes = document.getElementById("groupboxModifySingleRes");
	var hboxResId = document.createElement("hbox");
	var labelResId = document.createElement("label");
	if(window.arguments[0].isInterface == "true"){
		labelResId.setAttribute("value", "Interface id: ");
	}
	else
		labelResId.setAttribute("value", "Instance id: ");
	hboxResId.appendChild(labelResId);
	var textboxResId = document.createElement("textbox");
	textboxResId.setAttribute("flex", "1");
	if(window.arguments[0].isInterface == "true")
		textboxResId.setAttribute("readonly", "true"); 
	var resId = window.arguments[0].resId;
	textboxResId.setAttribute("value", resId);
	textboxResId.setAttribute("oldValue", resId);
	hboxResId.appendChild(textboxResId);
	groupboxModifySingelRes.appendChild(hboxResId);
	
	var propList =  window.arguments[0].propList;
	for(var i=0; i<propList.length; ++i){
		var hboxProp = document.createElement("hbox");
		var labelPropId = document.createElement("label");
		labelPropId.setAttribute("value", propList[i].propId+": ");
		var textboxPropValue = document.createElement("textbox");
		textboxPropValue.setAttribute("flex", "1");
		textboxPropValue.setAttribute("propId", propList[i].propId);
		textboxPropValue.setAttribute("value", propList[i].propValue);
		textboxPropValue.setAttribute("oldValue", propList[i].propValue);
		var labelPropDescr = document.createElement("label");
		labelPropDescr.setAttribute("value", " ("+propList[i].propDescr+")");
		hboxProp.appendChild(labelPropId);
		hboxProp.appendChild(textboxPropValue);
		hboxProp.appendChild(labelPropDescr);
		groupboxModifySingelRes.appendChild(hboxProp);
	}
	
};

art_semanticturkey.ontoling.onOk = function(){
	var textboxes = document.getElementsByTagName("textbox");
	
	//First of all we check if the id waf changed
	var oldId = textboxes[0].getAttribute("oldValue");
	var newId = textboxes[0].value;
	if(oldId != newId){
		window.arguments[0].modificationMade = "true";
		try {
			var responseXML = art_semanticturkey.ontoling.OLRequests.OntoLing.updateLingInstanceId(
					oldId,
					newId);
		}
		catch (e) {
			alert(e.name + ": " + e.message);
		}
	}
	//Then we examine each property
	for(var i=1; i<textboxes.length; ++i){
		var oldPropValue =  textboxes[i].getAttribute("oldValue");;
		var newPropValue = textboxes[i].value;
		if(oldPropValue != newPropValue){
			window.arguments[0].modificationMade = "true";
			var propId = textboxes[i].getAttribute("propId");
			var isIntefaceProp = window.arguments[0].isInterface;
			var interfaceId = window.arguments[0].interfaceId;
			try {
				var responseXML = art_semanticturkey.ontoling.OLRequests.OntoLing.updateLingInstancePropValue(
						newId,
						interfaceId,
						propId,
						newPropValue,
						isIntefaceProp);
				if(responseXML.getElementsByTagName("exception") == null){
					alert("The Linguisctic resource could not be loaded");
				}
			}
			catch (e) {
				alert(e.name + ": " + e.message);
			}
		}
	}
	close();
};

art_semanticturkey.ontoling.onCancel = function(){
	close();
};