/*
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at http://www.mozilla.org/MPL/
 * 
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 * 
 * The Original Code is SemanticTurkey.
 * 
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 * 
 * SemanticTurkey was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata (ART) Current
 * information about SemanticTurkey can be obtained at
 * http://semanticturkey.uniroma2.it
 * 
 */

if (typeof art_semanticturkey == 'undefined') var art_semanticturkey = {};
if (!art_semanticturkey.ontoling) art_semanticturkey.ontoling = {};
//if (!art_semanticturkey.ontoling.tabWindow) art_semanticturkey.ontoling.tabWindow = {};


window.onload = function() {
	document.getElementById("lemmaChosen").addEventListener("click",
			art_semanticturkey.ontoling.onAcceptLemma, true);
	document.getElementById("cancel").addEventListener("click",
			art_semanticturkey.ontoling.onClose, true);
	
	art_semanticturkey.ontoling.onLoadLemma();
};


art_semanticturkey.ontoling.onLoadLemma = function(){
	var lemmaList = window.arguments[0].lemmaList;
	//art_semanticturkey.ontoling.tabWindow = window.arguments[1];
	var radioGroupLemma = document.getElementById("radioGroupLemma");
	for(var i=0; i < lemmaList.length; ++i){
		var radioLemma = document.createElement('radio');
		var lemma = lemmaList[i].getAttribute("value");
		radioLemma.setAttribute("label", lemma);
		if(i==0){
			radioLemma.setAttribute("selected", true);
		}
		radioGroupLemma.appendChild(radioLemma);
	}
};

art_semanticturkey.ontoling.onClose = function() {
	close();
};


art_semanticturkey.ontoling.onAcceptLemma = function(){
	var radioGroupLemma = document.getElementById("radioGroupLemma");
	var selectedItem = radioGroupLemma.selectedItem;
	var lemma = selectedItem.getAttribute("label");
	var tabWindow = window.arguments[0].tabWindow;
	close();
	tabWindow.art_semanticturkey.ontoling.searchLemma(lemma);
};
