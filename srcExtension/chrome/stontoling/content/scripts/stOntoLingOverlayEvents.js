/*
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at http://www.mozilla.org/MPL/
 * 
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 * 
 * The Original Code is SemanticTurkey.
 * 
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 * 
 * SemanticTurkey was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata (ART) Current
 * information about SemanticTurkey can be obtained at
 * http://semanticturkey.uniroma2.it
 * 
 */
art_semanticturkey.ontoling.associateEventsSTOntoLingOverlay = function(){
	
	document.getElementById("menu_ToolsPopup").addEventListener("popupshowing",
			art_semanticturkey.ontoling.chkST_started,true);
	document.getElementById("menupopup_stOntLing").addEventListener("popupshowing",
			art_semanticturkey.ontoling.checkOntoLingUp, true);
	
	document.getElementById("STOntoLingstartMenuItem").addEventListener("command",
			art_semanticturkey.ontoling.startSTOntoLing, true);
	document.getElementById("STOntoLingtoolsmenu").addEventListener("command",
			art_semanticturkey.ontoling.loadOntoLing, true);
	document.getElementById("STOntoLingLoadResMenuItem").addEventListener("command",
			art_semanticturkey.ontoling.getAllLoadedRes, true);
	document.getElementById("STOntoLingReqLInst").addEventListener("command",
			art_semanticturkey.ontoling.getAllResource, true);
};


/** *********************************************************************** */
window.addEventListener("load",
		art_semanticturkey.ontoling.associateEventsSTOntoLingOverlay, true);
