/*
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at http://www.mozilla.org/MPL/
 * 
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 * 
 * The Original Code is SemanticTurkey.
 * 
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 * 
 * SemanticTurkey was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata (ART) Current
 * information about SemanticTurkey can be obtained at
 * http://semanticturkey.uniroma2.it
 * 
 */
//if (typeof art_semanticturkey == 'undefined') var art_semanticturkey = {};
if (!art_semanticturkey.ontoling)
	art_semanticturkey.ontoling = {};

Components.utils.import("resource://ontolingmodules/stOntoLingModule.jsm",
		art_semanticturkey.ontoling);

art_semanticturkey.ontoling.getResourceTree = function(){
	return document.getElementById("propertiesTree");
};

art_semanticturkey.ontoling.getSelectedTreeCell = function(){
	var tree = art_semanticturkey.ontoling.getResourceTree();
	var currentelement = tree.treeBoxObject.view
			.getItemAtIndex(tree.currentIndex);
	var treerow = currentelement.getElementsByTagName('treerow')[0];
	var treecell = treerow.getElementsByTagName('treecell')[0];
	return treecell;
};

art_semanticturkey.ontoling.loadSTOntolingSidebarPopUpProperty = function() {
	document.getElementById("clipmenu").addEventListener("popupshowing",
			art_semanticturkey.ontoling.enableSTOntoLingMenuProperty, false);
};

art_semanticturkey.ontoling.enableSTOntoLingMenuProperty = function() {
	// var mainWindow = getMainWindow();
	var stOntoLingMenuClassSidebarNode = document
			.getElementById("STOntoLingMenuPropertySidebar");
	if (art_semanticturkey.ontoling.stolModule.selRel == null) {
		stOntoLingMenuClassSidebarNode.disabled = true;
		return;
	}
	stOntoLingMenuClassSidebarNode.disabled = false;
	document.getElementById("STOntoLingMenupopupPropertySidebar")
			.addEventListener("popupshowing",
					art_semanticturkey.ontoling.dynamicAddingMenuPropertyItem,
					false);
};

art_semanticturkey.ontoling.dynamicAddingMenuPropertyItem = function() {
	var menupopupNode = document
			.getElementById("STOntoLingMenupopupPropertySidebar");
	var tabWindow = art_semanticturkey.ontoling.getTabWindow();
	// var mainWindow = getMainWindow();

	// remove all the previous menuitem
	while (menupopupNode.childNodes[0]) {
		menupopupNode.removeChild(menupopupNode.childNodes[0]);
	}

	if (tabWindow == null) {
		var menuItemNode = document.createElement("menuitem");
		menuItemNode.setAttribute("label", "open stOntoLing tab");
		menuItemNode
				.setAttribute(
						"oncommand",
						"art_semanticturkey.ontoling.getMainWindow().art_semanticturkey.ontoling.loadOntoLing();");
		menupopupNode.appendChild(menuItemNode);
		return;
	}
	if (tabWindow.art_semanticturkey.ontoling.selResInTab != art_semanticturkey.ontoling.stolModule.selRel) {
		var menuItemNode = document.createElement("menuitem");
		menuItemNode.setAttribute("label", "reload stOntoLing tab");
		menuItemNode
				.setAttribute(
						"oncommand",
						"art_semanticturkey.ontoling.getTabWindow().art_semanticturkey.ontoling.loadTab();");
		menupopupNode.appendChild(menuItemNode);
		return;
	}

	if (art_semanticturkey.ontoling.stolModule.ctxPropertyMenu.getLabelsAndIdArray.length == null) {
		return;
	}
	var selectedResURI = "";
	var selectedResLocal = "";
	if (document.getElementById("propertiesTree").currentIndex != -1) {
		//selectedRes should containt the local name of the resourse, not the complete URI, so this can be consedered as a temporary solution
		selectedResURI = document.getElementById("propertiesTree").treeBoxObject.view
				.getItemAtIndex(
						document.getElementById("propertiesTree").currentIndex)
				.getAttribute("propertyName");
	/*
	 * .getElementsByTagName('treerow')[0] .getElementsByTagName('treecell')[0]
	 * .getAttribute("label");
	 */
	
	
		var indexSemiCol = selectedResURI.indexOf(":");
		var indexHash = selectedResURI.lastIndexOf("#");
		var indexLocalName;
		if(indexHash > indexSemiCol)
			indexLocalName = indexHash+1;
		else if(indexHash < indexSemiCol)
			indexLocalName = indexSemiCol+1;
		else
			indexLocalName = 0;
		
		selectedResLocal = selectedResURI.substring(indexLocalName);
	}
	
	var labelsAndIds = art_semanticturkey.ontoling.stolModule.ctxPropertyMenu
			.getLabelsAndIdArray();
	var propType = document.getElementById("propertiesTree").treeBoxObject.view
			.getItemAtIndex(
					document.getElementById("propertiesTree").currentIndex)
			.getElementsByTagName('treerow')[0]
			.getElementsByTagName('treecell')[0].getAttribute("type");
			
	var languageLRes = art_semanticturkey.ontoling.stolModule.langLRes;		
			
	for ( var i = 0; i < labelsAndIds.length; ++i) {
		var menuitemNode = art_semanticturkey.ontoling.getMenuItemToBeAdded(
				labelsAndIds[i].getId(), labelsAndIds[i].getLabel(),
				selectedResURI, selectedResLocal, "property", languageLRes, propType);
		/*
		 * var menuitemNode = document.createElement("menuitem");
		 * menuitemNode.setAttribute("label", labelsAndIds[i].getLabel());
		 * menuitemNode.setAttribute("id", labelsAndIds[i].getId());
		 */
		menupopupNode.appendChild(menuitemNode);
	}
};

/** *********************************************************************** */
window.addEventListener("load",
		art_semanticturkey.ontoling.loadSTOntolingSidebarPopUpProperty, true);
