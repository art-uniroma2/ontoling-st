/*
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at http://www.mozilla.org/MPL/
 * 
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 * 
 * The Original Code is SemanticTurkey.
 * 
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 * 
 * SemanticTurkey was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata (ART) Current
 * information about SemanticTurkey can be obtained at
 * http://semanticturkey.uniroma2.it
 * 
 */
Components.utils.import("resource://stmodules/SemTurkeyHTTP.jsm");

EXPORTED_SYMBOLS = [ "HttpMgr", "OLRequests"];

OLRequests = new Object();

OLRequests.OntoLing = new Object();

//stontoling service requests
OLRequests.OntoLing.serviceName = "stOntoLing";
OLRequests.OntoLing.isOntoLingUpRequest = "isOntoLingUp";
OLRequests.OntoLing.startOntoLingRequest = "startOntoLing";
OLRequests.OntoLing.getLoadedLIntRequest = "getLoadedLInt";
OLRequests.OntoLing.getPossibleLIntRequest = "getPossibleLInt";
OLRequests.OntoLing.selectLingResRequest = "selectLingRes";
OLRequests.OntoLing.loadLingResRequest = "loadLingRes";
OLRequests.OntoLing.unloadLingResRequest = "unloadLingRes";
OLRequests.OntoLing.searchWordRequest = "searchWord";
OLRequests.OntoLing.searchLemmaRequest = "searchLemma";
OLRequests.OntoLing.lexRelRequest = "lexRel";
OLRequests.OntoLing.semRelRequest = "semRel";
OLRequests.OntoLing.addSubConceptsRequest = "addSubConcepts"; 
OLRequests.OntoLing.getLingInterfacesListRequest = "getLingInterfacesList";
OLRequests.OntoLing.getLingInstancesListRequest = "getLingInstancesList";
OLRequests.OntoLing.updateLingInstanceIdRequest = "updateLingInstanceId";
OLRequests.OntoLing.updateLingInstancePropValueRequest = "updateLingInstancePropValue";
OLRequests.OntoLing.createLingInstanceRequest = "createLingInstance";
OLRequests.OntoLing.removeLingInstanceRequest = "removeLingInstance";
