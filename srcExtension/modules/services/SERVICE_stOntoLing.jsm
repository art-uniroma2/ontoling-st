/*
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at http://www.mozilla.org/MPL/
 * 
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 * 
 * The Original Code is SemanticTurkey.
 * 
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 * 
 * SemanticTurkey was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata (ART) Current
 * information about SemanticTurkey can be obtained at
 * http://semanticturkey.uniroma2.it
 * 
 */
Components.utils.import("resource://ontolingmodules/OLRequests.jsm");
Components.utils.import("resource://stmodules/Logger.jsm");
Components.utils.import("resource://stmodules/Preferences.jsm");
Components.utils.import("resource://stmodules/Deserializer.jsm");	
Components.utils.import("resource://stmodules/ARTResources.jsm");

EXPORTED_SYMBOLS = [ "HttpMgr", "OLRequests" ];

var service = OLRequests.OntoLing;
var serviceName = service.serviceName;


/*
OLRequests.OntoLing.serviceName = "stOntoLing";
OLRequests.OntoLing.isOntoLingUpRequest = "isOntoLingUp";
OLRequests.OntoLing.startOntoLingRequest = "startOntoLing";
OLRequests.OntoLing.getLoadedLIntRequest = "getLoadedLInt";
OLRequests.OntoLing.getPossibleLIntRequest = "getPossibleLInt";
OLRequests.OntoLing.selectResRequest = "selectRes";
OLRequests.OntoLing.loadLingResRequest = "loadLingRes";
OLRequests.OntoLing.unloadLingResRequest = "unloadLingRes";
OLRequests.OntoLing.searchWordRequest = "searchWord";
OLRequests.OntoLing.searchLemmaRequest = "searchLemma";
OLRequests.OntoLing.lexRelRequest = "lexRel";
OLRequests.OntoLing.semRelRequest = "semRel";
OLRequests.OntoLing.addSubConceptsRequest = "addSubConcepts"; 
OLRequests.OntoLing.getLingInterfacesListRequest = "getLingInterfacesList";
OLRequests.OntoLing.getLingInstancesListRequest = "getLingInstancesList";
OLRequests.OntoLing.updateLingInstanceIdRequest = "updateLingInstanceId";
OLRequests.OntoLing.updateLingInstancePropValueRequest = "updateLingInstancePropValue";
OLRequests.OntoLing.createLingInstanceRequest = "createLingInstance";
OLRequests.OntoLing.removeLingInstanceRequest = "removeLingInstance";
*/

/**
 * ask the server if OntoLing is already being loaded
 * 
 * @return
 */
function isOntoLingUp() {
	Logger.debug('[SERVICE_stOntoLing.jsm] isOntoLingUp');
	return HttpMgr.GET(serviceName, service.isOntoLingUpRequest);
}

/**
 * start the OntoLing service
 * 
 * @return
 */
function startOntoLing() {
	Logger.debug('[SERVICE_stOntoLing.jsm] startOntoLing');
	var firstStart;
	var isFirstStart = Preferences.get("extensions.semturkey.stontoling.firststart", true);
	if(isFirstStart == true){
		Preferences.set("extensions.semturkey.stontoling.firststart", false);
		firstStart = "firstStart=true";
		}
	else
		firstStart = "firstStart=false";
	return HttpMgr.GET(serviceName, service.startOntoLingRequest, firstStart);
}

/**
 * get all the loaded linguistic Resources
 * 
 * @return
 */
function getLoadedLInt() {
	Logger.debug('[SERVICE_stOntoLing.jsm] getLoadedLIntRequest');
	return HttpMgr.GET(serviceName, service.getLoadedLIntRequest);
}

/**
 * get all the possible linguistic Resources
 * 
 * @return
 */
function getPossibleLInt() {
	Logger.debug('[SERVICE_stOntoLing.jsm] getPossibleLInt');
	return HttpMgr.GET(serviceName, service.getPossibleLIntRequest);
}

/**
 * select a  linguistic Resources
 * 
 * @return
 */
function selectLingRes(lResId) {
	Logger.debug('[SERVICE_stOntoLing.jsm] selectLingRes');
	var lResId = "lResId="+lResId;
	return HttpMgr.GET(serviceName, service.selectLingResRequest, lResId);
}

/**
 * load a linguistic Resources
 * 
 * @return
 */
function loadLingRes(lResId) {
	Logger.debug('[SERVICE_stOntoLing.jsm] loadLingRes');
	var lResId = "lResId="+lResId;
	return HttpMgr.GET(serviceName, service.loadLingResRequest, lResId);
}

/**
 * unload a linguistic Resources
 * 
 * @return
 */
function unloadLingRes(lResId) {
	Logger.debug('[SERVICE_stOntoLing.jsm] unloadLingRes');
	var lResId = "lResId="+lResId;
	return HttpMgr.GET(serviceName, service.unloadLingResRequest, lResId);
}

/**
 * search a word
 * 
 * @return
 */
function searchWord(word, srchStr, filtersArray) {
	Logger.debug('[SERVICE_stOntoLing.jsm] searchLemma');
	var word = "word="+word;
	var srchStr = "srchStr="+srchStr;
	var numFilters = "numFilters="+filtersArray.length;
	var filters="";
	filters = "filters=null"
	for(var i = 0; i<filtersArray.length; ++i){
		if(i == 0)
			filters = "filters="+filtersArray[i];
		else
			filters += "|_|"+filtersArray[i];
	}
	return HttpMgr.GET(serviceName, service.searchWordRequest, word, srchStr, numFilters, filters);
}

/**
 * search a lemma
 * 
 * @return
 */
function searchLemma(lemma) {
	Logger.debug('[SERVICE_stOntoLing.jsm] searchLemma');
	var lemma = "lemma="+lemma;
	return HttpMgr.GET(serviceName, service.searchLemmaRequest, lemma);
}

/**
 * navigate the linguistic resource using a lexical relation
 * 
 * @return
 */
function lexRel(lemma, semex, lexRel) {
	Logger.debug('[SERVICE_stOntoLing.jsm] lexRel');
	var lemma = "lemma=" + lemma;
	var semex = "semex=" + semex;
	var lexRel = "lexRel=" + lexRel;
	return HttpMgr.GET(serviceName, service.lexRelRequest, lemma, semex, lexRel);
}

/**
 * navigate the linguistic resource using a seamantic relation
 * 
 * @return
 */
function semRel(semex, semRel) {
	Logger.debug('[SERVICE_stOntoLing.jsm] semRel');
	var semex = "semex=" + semex;
	var semRel = "semRel=" + semRel;
	return HttpMgr.GET(serviceName, service.semRelRequest, semex, semRel);
}


/**
 * add the sub concept of a semex from the selected linguistic resources to a concept of the ontology
 * 
 * @return
 */
function addSubConcepts(semex, conceptId, type) {
	Logger.debug('[SERVICE_stOntoLing.jsm] addSubConcepts');
	var semex = "semex=" + semex;
	var conceptId = "conceptId=" + conceptId;
	var type = "type="+type;
	var resArray = new Array();
	
	var reply = HttpMgr.GET(serviceName, service.addSubConceptsRequest, semex, conceptId, type);
	resArray["concept"] = Deserializer.createURI(reply.getElementsByTagName("Concept")[0]);
	resArray["subConcepts"] = Deserializer.createRDFArray(reply.getElementsByTagName("SubConcepts")[0]);
	return resArray;
	
}


function getLingInterfacesList(){
	return HttpMgr.GET(serviceName, service.getLingInterfacesListRequest); 
}

function getLingInstancesList(interfaceId){
	var interfaceId = "interfaceId="+interfaceId;
	return HttpMgr.GET(serviceName, service.getLingInstancesListRequest, interfaceId); 
}

function updateLingInstanceId(oldInstanceId, newInstanceId){
	var oldInstanceId = "oldInstanceId="+oldInstanceId;
	var newInstanceId = "newInstanceId="+newInstanceId;
	return HttpMgr.GET(serviceName, service.updateLingInstanceIdRequest, oldInstanceId, newInstanceId); 
}

function updateLingInstancePropValue(instanceId, interfaceId, propId, propValue, isIntProp){
	var instanceId = "instanceId="+instanceId;
	var interfaceId = "interfaceId="+interfaceId;
	var propId = "propId="+propId;
	var propValue = "propValue="+propValue;
	var isIntProp = "isIntProp="+isIntProp; // This should be "true" or "false"
	return HttpMgr.GET(serviceName, service.updateLingInstancePropValueRequest, instanceId, interfaceId, propId, propValue, isIntProp); 
}

function createLingInstance(interfaceId, instanceId){
	var interfaceId = "interfaceId="+interfaceId;
	var instanceId = "instanceId="+instanceId;
	return HttpMgr.GET(serviceName, service.createLingInstanceRequest, interfaceId, instanceId); 
}

function removeLingInstance(interfaceId, instanceId){
	var interfaceId = "interfaceId="+interfaceId;
	var instanceId = "instanceId="+instanceId;
	return HttpMgr.GET(serviceName, service.removeLingInstanceRequest, interfaceId, instanceId); 
}


//stOntoLing SERVICE INITIALIZATION
service.isOntoLingUp = isOntoLingUp;
service.startOntoLing = startOntoLing;
service.getLoadedLInt = getLoadedLInt;
service.getPossibleLInt = getPossibleLInt;
service.selectLingRes = selectLingRes;
service.loadLingRes = loadLingRes;
service.unloadLingRes = unloadLingRes;
service.searchWord = searchWord;
service.searchLemma = searchLemma;
service.lexRel = lexRel;
service.semRel = semRel;
service.addSubConcepts = addSubConcepts;
service.getLingInterfacesList = getLingInterfacesList;
service.getLingInstancesList = getLingInstancesList;
service.updateLingInstanceId = updateLingInstanceId;
service.updateLingInstancePropValue = updateLingInstancePropValue;
service.createLingInstance = createLingInstance;
service.removeLingInstance = removeLingInstance;